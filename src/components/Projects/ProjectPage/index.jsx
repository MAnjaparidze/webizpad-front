import React, { useEffect, useState, useContext, useRef } from 'react';

import { getProjects, addProject, getProject } from '../../../services/projectsService';
import { getPlans, addPlan } from '../../../services/plansService';
import { getCases, addCase } from '../../../services/casesService';

import NavBar from '../../ReusableComponents/NavBar/index';
import Header from '../../ReusableComponents/Header/index';
import ContentPage from '../Parts/contentPage';
import { UserContext } from '../../../contexts/UserContext';

export default function Index(props) {
    const [projects, setProjects] = useState();
    const [currentProject, setCurrentProject] = useState(null);
    const [currentCases, setCurrentCases] = useState(null);
    const [currentPlans, setCurrentPlans] = useState(null);

    const { user } = useContext(UserContext);
    const { location } = props;

    useEffect(() => {
        if (user) {
            handleGetProjects(user);
        }
    }, [user])

    useEffect(() => {
        if (currentProject) {
            handleGetCases(currentProject._id);
            handleGetPlans(currentProject._id);
        }
    }, [currentProject])

    useEffect(() => {
        if (prevAmount !== location.pathname) {
            handlePageContent();
        }
    }, [location])

    const usePrevious = (value) => {
        const ref = useRef();
        useEffect(() => {
            ref.current = value;
        });
        return ref.current;
    }

    const prevAmount = usePrevious(location.pathname);

    const handlePageContent = async () => {
        let url = window.location.href;
        let start = url.indexOf("/projects/project/");
        let end = url.length;
        let itemId = url.substr(start + 18, end);
        handleGetProject(itemId);
    }

    const handleGetProjects = async (userObj) => {
        const { data } = await getProjects(userObj);
        setProjects(data);
    }

    const handleGetProject = async (projectID) => {
        const { data } = await getProject(projectID);
        setCurrentProject(data);
    }

    const handleGetPlans = async (id) => {
        const { data } = await getPlans({ parentID: id });
        setCurrentPlans(data);
    }

    const handleAddPlan = async (item) => {
        await addPlan(item)
        await handleGetPlans(currentProject._id);
    }

    const handleGetCases = async (id) => {
        const { data } = await getCases({ parentID: id });
        setCurrentCases(data);
    }

    const handleAddCase = async (item) => {
        await addCase(item);
        await handleGetCases(currentProject._id);
    }

    return (
        <div className='container'>
            <NavBar
                projects={projects}
                user={user}
                currentProject={currentProject}
                setCurrentProject={setCurrentProject}
                handleGetProjects={handleGetProjects}
                handleGetCases={handleGetCases}
                addProject={addProject}
            />
            <div className="content-container">
                <Header currentProject={currentProject} user={user} />
                <ContentPage
                    project={currentProject}
                    plans={currentPlans}
                    cases={currentCases}
                    handleAddCase={handleAddCase}
                    handleAddPlan={handleAddPlan}
                />
            </div>
        </div>
    )
}
