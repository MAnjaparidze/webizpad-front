import React, { useEffect, useState, useContext } from 'react';

import { getProjects, addProject } from '../../services/projectsService';
// import { getPlans, addPlan } from '../../services/plansService';
// import { getCases, addCase } from '../../services/casesService';

import NavBar from '../ReusableComponents/NavBar/index';
import Header from '../ReusableComponents/Header/index';
// import ContentPage from './Parts/contentPage';
import { UserContext } from './../../contexts/UserContext';

export default function Index() {
    const [projects, setProjects] = useState();

    const { user, handleUser } = useContext(UserContext);

    useEffect(() => {
        handleUser();
    },[])

    useEffect(() => {
        if (user) {
            handleGetProjects(user)
        }
    }, [user])

    const handleGetProjects = async (userObj) => {
        const { data } = await getProjects(userObj);
        setProjects(data);
    }

    return (
        <div className='container'>
            <NavBar
                user={user}
                projects={projects}
                handleGetProjects={handleGetProjects}
                addProject={addProject}
            />
            <div className="content-container">
                <Header
                    user={user}
                />
            </div>
        </div>
    )
}
