import React, { useState, useEffect, useRef } from 'react'

import { Joi } from '@hapi/joi';

import ArrowDownIcon from '../../../assets/icons/arrow-down.png';
import ProjectTableItem from '../../ReusableComponents/Project-Table-Item/index';

import InputItem from '../../ReusableComponents/Input/Input';

import '../Styles/contentPage.css';

export default function ContentPage({ project, plans, cases, handleAddCase, handleAddPlan }) {
    const [isCaseActive, setCaseActive] = useState(false);
    const [isPlanActive, setPlanActive] = useState(false);

    const [isFormActive, setFormActive] = useState(false);
    const [type, setType] = useState(null);

    const [itemName, setItemName] = useState("");

    useEffect(() => {
        if (isFormActive) document.getElementsByClassName("form-control")[0].focus();
    }, [isFormActive])

    const handleCaseClick = () => {
        setCaseActive(!isCaseActive);
        setPlanActive(false);
    }
    const handlePlanClick = () => {
        setPlanActive(!isPlanActive);
        setCaseActive(false);
    }

    const handleAddBtnClick = (type) => {
        setType(type);
        setFormActive(true);
    }

    const handleFormCancel = (e) => {
        e.preventDefault();
        setFormActive(false);
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        let item = {
            name: itemName,
            parentID: project._id,
            testNum: 0,
            passNum: 0,
            failNum: 0,
            queryNum: 0,
            blockedNum: 0,
            excludeNum: 0
        };
        if (type === "Case") {
            handleAddCase(item);
        } else if (type === 'Plan') {
            handleAddPlan(item);
        }
        setFormActive(false);

    }

    const handleChange = (e) => {
        e.preventDefault();
        setItemName(e.target.value)
    }

    function useOutsideClick(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setCaseActive(false);
                    setPlanActive(false);
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    function ContentButtons() {
        const wrapperRef = useRef(null);
        useOutsideClick(wrapperRef);

        return (
            <div ref={wrapperRef} className="content__buttons-container">
                <div className="new-case__dropdown">
                    <button className={project ? "drop__btn" : "drop__btn drop__btn--inactive"} disabled={project ? false : true} onClick={() => handleCaseClick()}>New Case <img src={ArrowDownIcon} alt="" /></button>
                    <div className={isCaseActive ? "dropdown-content dropdown-content--active" : "dropdown-content"}>
                        <button onClick={() => handleAddBtnClick("Case")}>+ New Case</button>
                        <button>- Dummy Button</button>
                    </div>
                </div>
                <div className="new-plan__dropdown">
                    <button className={project ? "drop__btn" : "drop__btn drop__btn--inactive"} disabled={project ? false : true} onClick={() => handlePlanClick()}>New Plan <img src={ArrowDownIcon} alt="" /></button>
                    <div className={isPlanActive ? "dropdown-content dropdown-content--active" : "dropdown-content"}>
                        <button onClick={() => handleAddBtnClick("Plan")}>+ New Plan</button>
                        <button>Test Button 2</button>
                    </div>
                </div>
            </div>
        );
    }

    // const schema = Joi.object({
    //     itemname: Joi.string().min(3).max(100).required()
    // })

    return (
        <div className="content-wrapper">
            {ContentButtons()}
            <div className="content">
                <div className="content-divider-plans">
                    {plans && plans.map((item) => {
                        return <ProjectTableItem key={item._id} linkTo={"plans"} item={item} project={project} />
                    })}
                </div>
                <div className="content-divider-cases">
                    {cases && cases.map((item) => {
                        return <ProjectTableItem key={item._id} linkTo={"cases"} item={item} project={project} />
                    })}
                </div>
                {isFormActive && <form className="submit__form" onSubmit={e => handleFormSubmit(e)}>
                    <h3>Create New Item</h3>
                    <InputItem type="text" name="caseName" label={`${type} Name`} value={itemName} onChange={handleChange} />
                    <div className="submit__form-btns">
                        <button className='form-cancel__btn' type="button" onClick={e => handleFormCancel(e)}>Cancel</button>
                        <button className='form-submit__btn' type="submit">Submit</button>
                    </div>
                </form>}
            </div>
        </div>
    )
}
