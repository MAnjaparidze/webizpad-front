import React from 'react'

export default function planProgresHeader({ item }) {
    return (
        <div className='plan-progress-header__wrapper'>
            <div className='item-name__wrapper'>
                <span className='item-name'>{item.name}</span>
            </div>
            <div className="item-progress__wrapper">
                <div className="item-progress-naming">
                    <span>Pass - {item.passNum}</span>
                    <span>Fail - {item.failNum}</span>
                    <span>Blocked - {item.blockedNum}</span>
                    <span>Query - {item.queryNum}</span>
                    <span>Exclude - {item.excludeNum}</span>
                    <span>{item.passNum} / {item.testNum}</span>
                    {/* <span>{item.passNum / item.textNum * 100}%</span> */}
                    <span>0%</span>
                </div>
                <div className='item-progress-bar'>
                    <div className="item-progress-bar--done"></div>
                </div>
            </div>
        </div>
    )
}
