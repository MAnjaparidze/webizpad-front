import React, { Component, useState, useEffect } from 'react'
import Joi from 'joi-browser';
import { forgotPassword } from '../../services/userService';
import { Link } from 'react-router-dom'

import CustomLoader from '../ReusableComponents/Loader/index';
import Input from '../ReusableComponents/Input/Input';
import LandingPageIMG from '../../assets/img/landing-page-form-image-cropped.png';

export default function ForgotPassword() {
    const [email, setEmail] = useState("");
    const [error, setError] = useState(null);
    const [message, setMessage] = useState(null);

    const [loading, setLoading] = useState(false);

    const schema = {
        email: Joi.string().min(8)
            .email({ minDomainSegments: 2 }).required().label("Email"),
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        let result = await forgotPassword(email);
        setLoading(false);
        setMessage(result.data.message);
    }

    const handleChange = ({ currentTarget: input }) => {
        const errors = { ...error };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];

        setEmail(input.value);
        setError(errors);
    }

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const cusSchema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, cusSchema);
        return error ? error.details[0].message : null;
    }

    return (
        <div className="container">
            <div className="landing-form-wrapper">
                <div className='form__container'>
                    <h3>Please Type Your Email</h3>
                    <form onSubmit={(e) => handleSubmit(e)} >
                        <Input name='email' value={email} label="Email" error={error ? error.email : null} type='text' onChange={handleChange} />
                        <div className='submit__status'>
                            {message && message}
                        </div>
                        <button type="submit" disabled={loading}>{loading ? <CustomLoader width={30} height={30} color={"#FFF"} type={"Puff"} timeout={3000} /> : "Send"}</button>
                    </form>
                    <Link to="/">Remembered Password? Sign In</Link>
                </div>
                <div className="landing-form-image-wrapper">
                    <img src={LandingPageIMG} alt="" className="landing-form-image" />
                </div>
            </div>
            <div className='landing-container-overlay'></div>
        </div>
    )
}
