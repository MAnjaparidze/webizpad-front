import React, { useEffect, useState } from 'react';
import Joi, { validate } from 'joi-browser';
import { Link } from 'react-router-dom';

import CustomLoader from '../ReusableComponents/Loader/index';
import Input from '../ReusableComponents/Input/Input';
import LandingPageIMG from '../../assets/img/landing-page-form-image-cropped.png';

import { verifyResetToken, resetPassword, forgotPassword } from '../../services/userService';

export default function ResetPassword() {
    const [isTokenValid, setIsTokenValid] = useState(false);
    const [message, setMessage] = useState(null);
    const [token, setToken] = useState(null);

    useEffect(() => {
        handleGetUrl();
    }, [])


    const handleGetUrl = async () => {
        let url = window.location.href;
        let start = url.indexOf("password/") + 9;
        let end = url.length;
        let token = url.substr(start, end)
        let result = await verifyResetToken(token);
        if (result.data.status === 200) {
            setMessage(result.data.message)
            setToken(token);
            setTimeout(() => {
                setIsTokenValid(true);
            }, 3000)
        } else if (result.data.status === 401) {
            setMessage(result.data.error);
        }
    }

    return (
        <div className='container'>
            <div className="landing-form-wrapper">
                {isTokenValid ?
                    <ChangePassword token={token} />
                    :
                    <div className="status-confirmation">
                        {message && message}
                    </div>}
                <div className="landing-form-image-wrapper">
                    <img src={LandingPageIMG} alt="" className="landing-form-image" />
                </div>
            </div>

        </div>
    )
}

function ChangePassword({ token }) {
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");
    const [message, setMessage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [submitError, setSubmitError] = useState(null);

    const [cusError, setError] = useState(null);

    const schema = {
        password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,64})/).error(() => { return { message: 'The password must be 8 to 64 characters long, contain at least one Upper & Lower Case letters, one number and one special character.' } }),
        repeat_password: Joi.any().valid(Joi.ref('password')).required().error(() => { return { message: "The passwords don't match." } }),
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const errorMessage = validate();
        setLoading(true);
        if (!errorMessage) {
            let result = await resetPassword(token, password);
            if (result.data.status === 200) {
                setMessage(result.data.message);
            } else {
                setSubmitError(result.data.error);
            }
        }
        setLoading(false);
    }

    const handlePasswordChange = ({ currentTarget: input }) => {
        const errors = { ...cusError };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        setPassword(input.value);
        setError(errors);
    }
    const handleRePasswordChange = ({ currentTarget: input }) => {
        const options = { abortEarly: false };
        const errors = { ...cusError };

        const { error } = Joi.validate({ password: password, repeat_password: input.value }, schema, options);
        if (error) {
            if (error.details.length === 2) {
                errors[input.name] = error.details[1].message;
            } else {
                errors[input.name] = error.details[0].message;
            }
        }
        else delete errors[input.name];
        setRePassword(input.value);
        setError(errors);
    }

    const validate = () => {
        const options = { abortEarly: false };

        const result = Joi.validate({ password: password, repeat_password: rePassword }, schema, options);

        if (!result.error) return null;

        const errors = {};
        for (let item of result.error.details)
            errors[item.path[0]] = item.message;

        return errors;
    }

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const cusSchema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, cusSchema);
        return error ? error.details[0].message : null;
    }

    return (
        <div className='form__container'>
            <h3>Please Type Your Email</h3>
            <form onSubmit={(e) => handleSubmit(e)} >
                <Input name='password' value={password} label="Password" error={cusError ? cusError.password : null} type='password' onChange={handlePasswordChange} />
                <Input name='repeat_password' value={rePassword} label="Repeat Password" error={cusError ? cusError.repeat_password : null} type='password' onChange={handleRePasswordChange} />
                <div className='submit__status'>
                    {submitError && submitError}
                    {message && <Link to='/auth'>{message}</Link>}
                </div>
                <button type="submit" disabled={loading}>{loading ? <CustomLoader width={30} height={30} color={"#FFF"} type={"Puff"} timeout={3000} /> : "Send"}</button>
            </form>
            <div className='landing-container-overlay'></div>
        </div>
    )
}