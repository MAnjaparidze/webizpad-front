import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getCase } from '../../services/casesService';
import { getCaseSteps, addCaseStep } from '../../services/caseStepsServices';
import { addCaseRun, getCaseRuns, getCaseRun } from '../../services/caseRunsServices';
import { addStepRunItem, getStepRunItems, getStepRunItem } from '../../services/caseRunItemsServices';
import { updateCaseRun, deleteCaseRun } from '../../services/caseRunsServices';

import { getPlan } from '../../services/plansService';
import { getProject } from '../../services/projectsService';

import CaseHeader from './Parts/caseHeader';
import AddNewRunModal from './Modals/AddNewRunModal';
import CaseRunColumn from './Parts/caseRunColumn';
import CaseRunItemModal from './Modals/CaseRunItemModal';

import PlusIcon_Orange from '../../assets/icons/plus-orange.png';

import './Styles/main.css';
import { block } from 'joi-browser';

export default function Index() {
    const [caseParentProject, setCaseParentProject] = useState(null);
    const [caseParentPlan, setCaseParentPlan] = useState(null);
    const [caseItem, setCaseItem] = useState(null);
    const [caseSteps, setCaseSteps] = useState([]);
    const [caseStepName, setCaseStepName] = useState("");
    const [caseRuns, setCaseRuns] = useState(null);
    const [caseRun, setCaseRun] = useState(null);
    const [isAddRunActive, setAddRunActive] = useState(false);
    const [caseRunItem, setCaseRunItem] = useState(null);
    const [caseRunItemActive, setCaseRunItemActive] = useState(false);
    const [parentRun, setParentRun] = useState(null);

    const [isInProgress, setIsInProgress] = useState(true);
    const [canBeRunBy, setCanBeRunBy] = useState([]);


    useEffect(() => {
        handleGetCaseItem();
    }, [])

    useEffect(() => {
        if (caseItem) {
            hadleGetCaseSteps(caseItem._id);
            handleGetCaseRuns(caseItem._id);
        }
    }, [caseItem])

    const handleGetCaseItem = async () => {
        let url = window.location.href;
        let start = url.indexOf("case") + 6;
        let end = url.length;
        let itemId = url.substr(start, end)
        let result = await getCase(itemId);
        setCaseItem(result.data);

        let planProject = await getPlan(result.data.parentID)
        if (!planProject.data) {
            let parentProject = await getProject(result.data.parentID);
            setCaseParentProject(parentProject.data);
        } else {
            setCaseParentPlan(planProject.data);
            let parentProject = await getProject(planProject.data.parentID);
            setCaseParentProject(parentProject.data);
        }
    }

    const handleAddCaseStep = async (e) => {
        e.preventDefault();
        let caseStepObject = {
            name: caseStepName,
            parentCaseID: caseItem._id,
        };
        let result = await addCaseStep(caseStepObject);
        hadleGetCaseSteps(caseItem._id)
        handleCreateStepRunViaStep(result.data)
        setCaseStepName("");
    }

    const hadleGetCaseSteps = async (caseID) => {
        let result = await getCaseSteps(caseID);
        setCaseSteps(result.data);
    }

    const handleCaseStepNameChange = (e) => {
        setCaseStepName(e.target.value);
    }

    const handleAddCaseRun = async () => {
        let caseRunObject = {
            inProgress: true,
            parentCase: caseItem._id,
            testerAlias: ["anyone"],
            tagFilter: [],
            build: "TBN",
            passNum: 0,
            failNum: 0,
            blockNum: 0,
            queryNum: 0,
            totalNum: caseSteps.length
        };
        let result = await addCaseRun(caseRunObject);
        handleCreateStepRunViaRun(result);
        setCaseRun(result);
    }

    const handleGetCaseRuns = async (caseID) => {
        let result = await getCaseRuns(caseID);
        setCaseRuns(result.data);
    }

    const handleAddRunClick = async (e) => {
        e.preventDefault();
        let result = await handleAddCaseRun();
        setAddRunActive(true);
    }

    const handleCreateStepRunViaRun = async (caseRun) => {
        caseSteps.forEach(async (step) => {
            const stepRunObj = {
                pass: false,
                fails: false,
                query: false,
                block: false,
                exclude: false,
                parentRun: caseRun._id,
                parentStep: step._id,
                comment: "",
                link: ""
            }
            await addStepRunItem(stepRunObj);
        })
        handleGetCaseRuns(caseItem._id);
    }
    const handleCreateStepRunViaStep = async (caseStep) => {
        await caseRuns.forEach(async (run) => {
            const stepRunObj = {
                pass: false,
                fails: false,
                query: false,
                block: false,
                exclude: false,
                parentRun: run._id,
                parentStep: caseStep._id,
                comment: "",
                link: ""
            }

            await addStepRunItem(stepRunObj);

            await handleRunColUpdate(run)
        })
        handleGetCaseRuns(caseItem._id);
    }

    const handleRunColumnClick = async (runCol) => {
        setCaseRun(runCol);
        setAddRunActive(true);
    }

    // const handleRunColUpdate = async (isInProgress, canBeRunBy) => {
    //     const caseRunObj = {
    //         _id: caseRun._id,
    //         inProgress: isInProgress,
    //         testerAlias: (canBeRunBy.length === 0 ? ["Anyone"] : canBeRunBy)
    //     }
    //     let result = await updateCaseRun(caseRunObj);
    //     setAddRunActive(false);
    // }

    const handleRunColUpdate = async (runId) => {
        // const caseRunObj = {
        //     _id: caseRun._id,
        //     inProgress: isInProgress,
        //     testerAlias: (canBeRunBy.length === 0 ? ["Anyone"] : canBeRunBy)
        // }
        let res = await getStepRunItems(runId._id)
        let passN = res.data.filter(e => { return e.pass === true }).length;
        let failsN = res.data.filter(e => { return e.fail === true }).length;
        let queryN = res.data.filter(e => { return e.query === true }).length;
        let blockN = res.data.filter(e => { return e.block === true }).length;
        let excludeN = res.data.filter(e => { return e.exclude === true }).length;

        let sum = passN + failsN + queryN + blockN + excludeN;
        let demoIsInProgress = true;
        if (sum === res.data.length) {
            console.log("Test", sum)
            demoIsInProgress = false
        }
        let caseRunObj = {
            inProgress: demoIsInProgress,
            testerAlias: canBeRunBy,
            totalNum: res.data.length - excludeN,
            passNum: passN,
            failNum: failsN,
            blockNum: blockN,
            queryNum: queryN,
        }

        const obj = {
            _id: runId,
            object: caseRunObj
        }
        let result = await updateCaseRun(obj);
        console.log(result);


        // let caseRes = await getCaseRuns(caseItem._id)

        // const getOveralData = (array, type) => {
        //     let result = 0;
        //     array.data.forEach(async (item) => {
        //         let res = await getStepRunItems(item._id);
        //         let test = res.data.filter(i => {
        //             switch (type) {
        //                 case "pass":
        //                     return i.pass === true;
        //                 case "fail":
        //                     return i.fail === true;
        //                 case "query":
        //                     return i.query === true;
        //                 case "block":
        //                     return i.block === true;
        //                 case "exclude":
        //                     return i.exclude === true;
        //                 default:
        //                     return 0;
        //             }
        //         }).length;
        //         console.log(test);
        //         return result += test;
        //         console.log(result, "Test 2")
        //     })
        //     return result;
        // }

        // console.log(caseRes, "<===== Ohohoh")
        // let passNM = getOveralData(caseRes, "pass");
        // let passNM = caseRes.data.map(async (item) => { let res = await getStepRunItems(item._id); return res.data.filter(item => { return item.pass === true }).length })
        // let passNM = res.data.filter(e => { return e.pass === true }).length;
        // console.log(passNM, "<====== God Please")
        // let failsNM = res.data.filter(e => { return e.fail === true }).length;
        // let queryNM = res.data.filter(e => { return e.query === true }).length;
        // let blockNM = res.data.filter(e => { return e.block === true }).length;
        // let excludeNM = res.data.filter(e => { return e.exclude === true }).length;



        // let caseObj = {
        //     testNum: caseItem.testNum,
        //     passNum: caseItem.passNum,
        //     failNum: caseItem.failNum,
        //     queryNum: caseItem.queryNum,
        //     blockedNum: caseItem.blockedNum,
        //     excludeNum: caseItem.excludeNum,
        // }
    }

    const handleRunColDelete = async (caseRunItem) => {
        let result = await deleteCaseRun(caseRunItem._id);
        handleGetCaseRuns(caseItem._id);
        setAddRunActive(false);
    }

    return (
        <div className='cases-page__container'>
            <CaseHeader />
            <div className="cases-page__body">
                <div className="cases-body__header">
                    <span className='cases-body-header__name'>
                        {/* {caseParentProject && caseParentProject.name}   */}
                        {caseParentProject &&  <Link to={`/projects/project/${caseParentProject._id}`}>{caseParentProject.name}</Link>}
                        {caseParentProject && " > "}
                        {(caseParentProject && caseParentPlan) &&  <Link to={`/projects/${caseParentProject._id}/plans/${caseParentPlan._id}`}>{caseParentPlan.name}</Link>}
                        {caseParentProject && " > "}
                        {`${caseItem && caseItem.name}`}
                    </span>
                    <div className="item-progress__wrapper">
                        {caseItem && <div className="item-progress-naming">
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--green'></div>
                                {caseItem.passNum}
                            </div>
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--red'></div>
                                {caseItem.failNum}
                            </div>
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--black'></div>
                                {caseItem.blockedNum}
                            </div>
                            <div className='item-naming__wrapper'>{caseItem.passNum} / {caseItem.testNum}</div>
                            {/* <div className='item-naming__wrapper'>{caseItem.passNum / caseItem.textNum * 100}%</div> */}
                            <div className='item-naming__wrapper'>0%</div>
                        </div>}
                        <div className='item-progress-bar'>
                            <div className="item-progress-bar--done"></div>
                        </div>
                    </div>
                </div>

                <div className="cases-body__table">
                    <div className="case-table__left">
                        <div className="case-table-left__head">
                            <h2>Case Run Table</h2>
                            <button onClick={(e) => handleAddRunClick(e)}>
                                <img src={PlusIcon_Orange} alt="" />
                                Add New Run
                            </button>
                        </div>
                        <div className="test-case__wrapper">
                            {caseSteps.map((caseStep, e) => {
                                return <div key={e} className="test-case">
                                    <div className="test-case__number">{e + 1}</div>
                                    <div className="test-case__name">{caseStep.name}</div>
                                </div>
                            })}
                            <form onSubmit={e => handleAddCaseStep(e)}>
                                <input placeholder="Add New Step" value={caseStepName} className='test-case__input' type="text" onChange={handleCaseStepNameChange} />
                            </form>
                        </div>
                    </div>
                    <div className="case-table__right">
                        {caseRuns && caseRuns.slice(0).reverse().map((item) => {
                            return <CaseRunColumn
                                key={item._id}
                                handleRunColumnClick={handleRunColumnClick}
                                item={item}
                                setCaseRun={setCaseRun}
                                caseSteps={caseSteps}
                                caseRunItem={caseRunItem}
                                setParentRun={setParentRun}
                                setCaseRunItem={setCaseRunItem}
                                setCaseRunItemActive={setCaseRunItemActive}
                            />
                        })}
                    </div>
                </div>
            </div>
            {caseRun && <AddNewRunModal
                isAddRunActive={isAddRunActive}
                caseRun={caseRun}
                caseSteps={caseSteps}
                setAddRunActive={setAddRunActive}
                handleRunColUpdate={handleRunColUpdate}
                handleRunColDelete={handleRunColDelete}
                isInProgress={isInProgress}
                setIsInProgress={setIsInProgress}
                canBeRunBy={canBeRunBy}
                setCanBeRunBy={setCanBeRunBy}
            />}
            {(caseRunItemActive && parentRun) && <CaseRunItemModal
                caseRunItem={caseRunItem}
                setCaseRunItem={setCaseRunItem}
                setCaseRunItemActive={setCaseRunItemActive}
                handleRunColUpdate={handleRunColUpdate}
                parentRun={parentRun}
                handleGetCaseRuns={handleGetCaseRuns}
                caseItem={caseItem}
            />}
        </div>
    )
}
