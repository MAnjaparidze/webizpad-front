import React, { useEffect, useState } from 'react';

import { getStepRunItem, getStepRunItems, updateSetRunItem } from '../../../services/caseRunItemsServices';
import { getCaseRun, updateCaseRun } from '../../../services/caseRunsServices';

import { useHistory } from 'react-router-dom';

import ProgressBar from '../../ReusableComponents/ProgressBar/index';

export default function CaseRunItemModal({ caseRunItem, parentRun, handleGetCaseRuns, handleRunColUpdate, setCaseRunItemActive, setCaseRunItem }) {
    const [caseRunItems, setCaseRunItems] = useState(null);
    const [itemComment, setItemComment] = useState("");
    const [itemLink, setItemLink] = useState("");
    const [inParentRun, setInParentRun] = useState(null);


    useEffect(() => {
        handleGetCaseRunItems();
        handleGetCaseRun();
        return () => {
            setCaseRunItems(null);
            setItemComment("");
            setItemLink("");
        }
    }, [])

    useEffect(() => {
        handleGetCaseRun();
    }, [parentRun])

    useEffect(() => {
        if (caseRunItem) {
            setItemComment(caseRunItem.comment);
            setItemLink(caseRunItem.link);
        }
        handleGetCaseRun();
    }, [caseRunItem])

    const handleGetCaseRunItems = async () => {
        let result = await getStepRunItems(parentRun);
        setCaseRunItems(result.data);
    }

    const handleNextItem = async () => {
        let result = await getStepRunItems(parentRun._id);
        let currIndex = result.data.map((e) => { return e._id }).indexOf(caseRunItem._id)
        if (currIndex < result.data.length - 1) {
            let nextCaseRunItem = await getStepRunItem(result.data[currIndex + 1]._id);
            setCaseRunItem(nextCaseRunItem.data);
        } else if (currIndex === result.data.length - 1) {
            let nextCaseRunItem = await getStepRunItem(result.data[currIndex]._id);
            setCaseRunItem(nextCaseRunItem.data);
            await handleGetCaseRuns(parentRun.parentCase);
        } else {
            // await handleGetCaseRuns(parentRun.parentCase);
        }
    }

    const handleGetCaseRun = async () => {
        let result = await getCaseRun(parentRun._id);
        setInParentRun(result.data)
    }

    const handleUpdateCaseRunItem = async (type) => {
        const caseRunItemObj = {
            _id: caseRunItem._id,
            pass: false,
            fail: false,
            query: false,
            block: false,
            exclude: false,
            comment: itemComment,
            link: itemLink
        }
        switch (type) {
            case "PASS":
                caseRunItemObj.pass = true;
                break;
            case "FAIL":
                caseRunItemObj.fail = true;
                break;
            case "BLOCKED":
                caseRunItemObj.block = true;
                break;
            case "QUERY":
                caseRunItemObj.query = true;
                break;
            case "EXCLUDE":
                caseRunItemObj.exclude = true;
                break;
            default:
                caseRunItemObj.pass = false;
                caseRunItemObj.fail = false;
                caseRunItemObj.query = false;
                caseRunItemObj.block = false;
                caseRunItemObj.exclude = false;
        }
        let result = await updateSetRunItem(caseRunItemObj);
        await handleRunColUpdate(parentRun);
        handleNextItem();
    }

    return (
        <div className='case-run-item__modal'>
            <div className="case-run-item__modal-header">
                <div className="case-run-item__modal-header-name">Run Item</div>
                <div className="case-run-item__modal-header-btns">
                    <div className="case-run-item__modal-header-esc-btn">Save</div>
                    <div className="case-run-item__modal-header-close-btn" onClick={() => { setCaseRunItemActive(false); setCaseRunItem(null) }}>Close</div>
                </div>
            </div>

            {inParentRun && <ProgressBar item={inParentRun} />}
            {(inParentRun && inParentRun.inProgress) && <textarea type="text" value={itemComment} className="case-run-item__modal-comment" onChange={e => setItemComment(e.target.value)} />}
            {(inParentRun && inParentRun.inProgress) && <input type="text" value={itemLink} className="case-run-item__modal-link" onChange={e => setItemLink(e.target.value)} />}
            {(inParentRun && inParentRun.inProgress) ? <div className="case-run-item__modal-btns">
                <div className="case-run-item__modal-main-btns">
                    <button className='case-run-item__modal-pass-btn' onClick={() => { handleUpdateCaseRunItem("PASS") }}>PASS</button>
                    <button className='case-run-item__modal-fail-btn' onClick={() => { handleUpdateCaseRunItem("FAIL") }}>FAIL</button>
                </div>
                <div className="case-run-item__modal-extra-btns">
                    <button className='case-run-item__modal-blocked-btn' onClick={() => { handleUpdateCaseRunItem("BLOCKED") }}>BLOCKED</button>
                    <button className='case-run-item__modal-query-btn' onClick={() => { handleUpdateCaseRunItem("QUERY") }}>QUERY</button>
                    <button className='case-run-item__modal-exclude-btn' onClick={() => { handleUpdateCaseRunItem("EXCLUDE") }}>EXCLUDE</button>
                </div>
                <div className="case-run-item__modal-auto-advance">
                    <div className="case-run-item__modal-auto-advance-select">
                        SELECT
                    </div>
                </div>
                <div className="case-run-item__modal-btns">
                    <button className='case-run-item__modal-clear-btn'>CLEAR</button>
                    <button className='case-run-item__modal-prev-btn'>PREV</button>
                    <button className='case-run-item__modal-next-btn' onClick={() => handleNextItem()}>NEXT</button>
                </div>
            </div> : <CaseRunItemModalComplete parentRun={parentRun} />}
        </div>
    )
}

function CaseRunItemModalComplete({ parentRun }) {

    const history = useHistory();

    const handleViewReportClick = () => {
        history.push(`/cases/report/${parentRun.parentCase}`);
    }

    return (
        <div className='case-run-item-modal--completed'>
            <h1>Completed</h1>

            <div className="case-run-item-modal--completed-btns">
                <button onClick={() => handleViewReportClick()}>View the Test Report</button>
                <button>Start a Retest</button>
                <button>Go Back to In-Progress</button>
            </div>
        </div>
    )
}
