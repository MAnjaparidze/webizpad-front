import React, { useState, useEffect } from 'react'


import Input from '../../ReusableComponents/Input/Input';

export default function AddNewRunModal({ isAddRunActive, isInProgress, setIsInProgress, canBeRunBy, setCanBeRunBy, setAddRunActive, caseRun, caseSteps, handleRunColDelete, handleRunColUpdate }) {
    // const [isInProgress, setIsInProgress] = useState(true);
    // const [canBeRunBy, setCanBeRunBy] = useState([]);

    useEffect(() => {
        setIsInProgress(caseRun.inProgress);
    }, [])

    return (
        <div className={isAddRunActive ? 'add-run__modal add-run__modal--active' : 'add-run__modal'}>
            <div className="add-run__header">
                <h3>Test Run Details</h3>
                <div className="add-run__header-btns">
                    <button className='add-run__btns-save' type='submit' onClick={() => handleRunColUpdate(caseRun._id)}>Save</button>
                    <button className='add-run__btns-delete' type='button' onClick={() => handleRunColDelete(caseRun)}>Delete this Run</button>
                    <button className='add-run__btns-cancel' type='button' onClick={() => setAddRunActive(false)}>Cancel</button>
                </div>
            </div>

            <div className="add-run__body">
                <div className="add-run__progress">
                    <div className='run-progress__label'>Progress of the Run: </div>
                    <div className="add-run__progress-btns">
                        <button
                            className={caseRun.inProgress ? 'progress-btns__inprogress inprogress-btn--active' : "progress-btns__inprogress"}
                            type='button'
                            onClick={() => setIsInProgress(true)}
                        >
                            INPROGRESS
                        </button>
                        <button
                            className={caseRun.inProgress ? 'progress-btns__completed' : "progress-btns__completed completed-btn--active"}
                            type='button'
                            onClick={() => setIsInProgress(false)}
                        >
                            COMPLETED
                        </button>
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Number: </div>
                    <div className="add-run__progress-info">
                        {caseRun && caseRun.queueNumber}
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Test Can Be Run By: </div>
                    <div className='run-progress__testerAlias'>
                        {/* {caseRun && caseRun.testerAlias.map((item, e) => {
                                return <div key={e} className='run-progress__testerAlias-item'>{item}</div>
                            })} */}
                        <div className='run-progress__testerAlias-item'>Anyone</div>
                    </div>
                </div>

                <div className="add-run__progress">
                    <div className='run-progress__label'>Tester Alias: </div>
                    <div className="add-run__progress-info">
                        <div className='run-progress__testerAlias'>
                            {caseRun && caseRun.testerAlias.map((item, e) => {
                                return <div key={e} className='run-progress__testerAlias-item'>{item}</div>
                            })}

                        </div>
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Date: </div>
                    <div className="add-run__progress-info">
                        {caseRun && caseRun.date}
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Time: </div>
                    <div className="add-run__progress-info">
                        {caseRun && caseRun.time.substr(0, 12)}
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Tag Filter: </div>
                    <div className="add-run__progress-info">
                        {caseRun && caseRun.tagFilter}
                    </div>
                </div>
                <div className="add-run__progress">
                    <div className='run-progress__label'>Build: </div>
                    <div className="add-run__progress-info">
                        {caseRun && caseRun.build}
                    </div>
                </div>
            </div>
        </div >
    )
}
