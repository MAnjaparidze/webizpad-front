import React, { useState, useEffect } from 'react'
import Joi from 'joi-browser';
import { getCase, updateCase } from '../../../services/casesService';
import { getCaseSteps } from '../../../services/caseStepsServices';
import { getCaseRuns } from '../../../services/caseRunsServices';

import AddPeopleIcon from '../../../assets/icons/add-person-white.png';
import Input from '../../ReusableComponents/Input/Input';

import ArrowDownBlack from '../../../assets/icons/arrow-down-black.png';
import DeleteWhite from '../../../assets/icons/delete-white.png';

import CaseHeader from '../Parts/caseHeader';
import CaseRunColumn from '../Parts/reportCaseRunColumn';
import ReportPageLogs from '../../ReusableComponents/ReportPageLogs/index';

import PlusIcon_Orange from '../../../assets/icons/plus-orange.png';

import '../Styles/main.css';

export default function Index() {
    const [caseItem, setCaseItem] = useState(null);
    const [caseSteps, setCaseSteps] = useState([]);
    const [caseStepName, setCaseStepName] = useState("");
    const [caseRuns, setCaseRuns] = useState(null);
    const [caseRun, setCaseRun] = useState(null);
    const [isAddRunActive, setAddRunActive] = useState(false);
    const [caseRunItem, setCaseRunItem] = useState(null);
    const [caseRunItemActive, setCaseRunItemActive] = useState(false);
    const [parentRun, setParentRun] = useState(null);
    const [shareModalActive, setShareModalActive] = useState(false);


    useEffect(() => {
        handleGetCaseItem();
    }, [])

    useEffect(() => {
        if (caseItem) {
            hadleGetCaseSteps(caseItem._id);
            handleGetCaseRuns(caseItem._id);
        }
    }, [caseItem])

    const handleGetCaseItem = async () => {
        let url = window.location.href;
        let start = url.indexOf("report") + 7;
        let end = url.length;
        let itemId = url.substr(start, end)
        let result = await getCase(itemId);
        setCaseItem(result.data);
    }

    const hadleGetCaseSteps = async (caseID) => {
        let result = await getCaseSteps(caseID);
        setCaseSteps(result.data);
    }

    const handleGetCaseRuns = async (caseID) => {
        let result = await getCaseRuns(caseID);
        setCaseRuns(result.data);
    }

    const handleShareReportClick = async (e) => {
        e.preventDefault();
        setShareModalActive(true);
    }

    return (
        <div className='cases-page__container case-report-page'>
            <CaseHeader />
            <div className="cases-page__body">
                <div className="cases-body__header">
                    <span className='cases-body-header__name'>{`${caseItem && caseItem.name}`}</span>
                    <div className="item-progress__wrapper">
                        {caseItem && <div className="item-progress-naming">
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--green'></div>
                                {caseItem.passNum}
                            </div>
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--red'></div>
                                {caseItem.failNum}
                            </div>
                            <div className='item-naming__wrapper'>
                                <div className='cases-item__indicator--black'></div>
                                {caseItem.blockedNum}
                            </div>
                            <div className='item-naming__wrapper'>{caseItem.passNum} / {caseItem.testNum}</div>
                            {/* <div className='item-naming__wrapper'>{caseItem.passNum / caseItem.textNum * 100}%</div> */}
                            <div className='item-naming__wrapper'>0%</div>
                        </div>}
                        <div className='item-progress-bar'>
                            <div className="item-progress-bar--done"></div>
                        </div>
                    </div>
                </div>

                <div className="cases-body__table">
                    <div className="case-table__left">
                        <div className="case-table-left__head">
                            <h2>Case Run Table</h2>
                            <button onClick={(e) => handleShareReportClick(e)}>
                                <img src={PlusIcon_Orange} alt="" />
                                Share Report
                            </button>
                        </div>
                        <div className="test-case__wrapper">
                            {caseSteps.map((caseStep, e) => {
                                return <div key={e} className="test-case">
                                    <div className="test-case__number">{e + 1}</div>
                                    <div className="test-case__name">{caseStep.name}</div>
                                </div>
                            })}

                        </div>
                    </div>
                    <div className="case-table__right">
                        {caseRuns && caseRuns.map((item) => {
                            return <CaseRunColumn
                                key={item._id}
                                item={item}
                                caseRunItem={caseRunItem}
                            />
                        })}
                    </div>
                </div>

                <div className="cases-report-page__logs">
                    <h2>Run Details</h2>
                    {caseRuns && caseRuns.map((item) => {
                        return <ReportPageLogs key={item._id} item={item} />
                    })}
                </div>
                {shareModalActive && <ShareReportModal item={caseItem} setShareModalActive={setShareModalActive} />}
            </div>
        </div>
    )
}

const ShareReportModal = ({ item, setShareModalActive }) => {
    const [personEmail, setPersonEmail] = useState("");
    const [chosenPersons, setChosenPersons] = useState([]);
    const [error, setError] = useState(null);

    const [sharedWithActive, setSharedWithActive] = useState(false);

    const schema = {
        email: Joi.string().min(8)
            .email({ minDomainSegments: 2 }).required().label("Email"),
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let personArray = [...chosenPersons];
        const errorMessage = validateProperty({ name: "email", value: personEmail });
        if (errorMessage) {
            console.log(error, error.length);
            return;
        } else {
            personArray.push(personEmail);
        }
        setChosenPersons(personArray);
    }

    const handleDeletePerson = (item) => {
        let demoArr = [...chosenPersons];
        let index = demoArr.indexOf(item);

        demoArr.splice(index, 1);
        setChosenPersons(demoArr);
    }

    const handleChange = ({ currentTarget: input }) => {
        const errors = { ...error };
        const errorMessage = validateProperty(input);
        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];
        setPersonEmail(input.value);
        setError(errors);
    }

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const cusSchema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, cusSchema);
        return error ? error.details[0].message : null;
    }

    const handleSave = () => {
        setShareModalActive(false)
    }

    const handleCancel = () => {
        setShareModalActive(false);
    }

    return (
        <div className="share-report-modal">
            <div className="share-report-header">
                <img src={AddPeopleIcon} alt="" />
                <span>Choose people to share with</span>
            </div>
            <div className="share-report-input">
                <form onSubmit={(e) => handleSubmit(e)}>
                    <Input value={personEmail} name="email" type="text" label="Add People and Groups" onChange={e => handleChange(e)} error={error ? error.email : null} />
                </form>
            </div>
            <span className='share-report-chosen-label'>Shared to:</span>
            <div className="share-report-chosen" name="share-report-chosen">
                <div className="share-report-chosen-first">
                    <div className="chosen-person-first">
                        {chosenPersons.length > 0 && chosenPersons[0]}
                    </div>
                    <img src={ArrowDownBlack} className={sharedWithActive ? 'chosen-list-toggle chosen-list-toggle--active' : 'chosen-list-toggle'} alt="" onClick={() => setSharedWithActive(!sharedWithActive)} />
                </div>
                {sharedWithActive && <div className="share-report-chosen-list">
                    {chosenPersons.length > 0 && chosenPersons.map((item, i) => {
                        return (
                            <div key={i} className='chosen-person-wrapper'>
                                <div className='chosen-person'>{item}</div>
                                <button onClick={() => handleDeletePerson(item)} className="chosen-person-delete">
                                    <img src={DeleteWhite} alt="" />
                                </button>
                            </div>
                        )
                    })}
                </div>}
            </div>
            <div className="share-report-buttons">
                <button className="share-report-save" onClick={() => handleSave()}>Save</button>
                <button className="share-report-cancel" onClick={() => handleCancel()}>Cancel</button>
            </div>
        </div>

    )
}