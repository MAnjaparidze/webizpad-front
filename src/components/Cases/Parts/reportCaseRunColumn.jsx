import React, { useEffect, useState, useRef } from 'react';

import { getStepRunItems, getStepRunItem } from '../../../services/caseRunItemsServices';

import CheckIcon from '../../../assets/icons/check.png';
import FailIcon from '../../../assets/icons/fail.png';
import QueryIcon from '../../../assets/icons/questionMark.png';
import BlockIcon from '../../../assets/icons/block.png';
import Exclude from '../../../assets/icons/minus-black.png';


export default function CaseRunColumn({ item, caseRunItem }) {
    const [stepRunItems, setStepRunItems] = useState(null);

    useEffect(() => {
        handleGetStepRunItems();
    }, [])

    // useEffect(() => {
    //     handleGetStepRunItems();
    // }, [caseSteps, caseRunItem])

    const handleGetStepRunItems = async () => {
        const result = await getStepRunItems(item._id);
        setStepRunItems(result.data);
    }

    return (
        <div className='case-run__column'>
            <div className="run-column__header" >
                <div className="run-column__id">
                    #{item.queueNumber}
                </div>
                <div className="run-column__date">
                    {item.date}
                </div>
            </div>
            <div className="run-column__body">
                {stepRunItems && stepRunItems.map((c) => {
                    return <RunColumnRow key={c._id} item={c} caseRun={item} caseRunItem={caseRunItem} />
                })}
                {!item.inProgress && <div className="run-column__body--completed" >
                    <div>Completed</div>
                </div>}
            </div>
        </div>
    )
}

function RunColumnRow({ item, caseRunItem }) {

    return (
        <div className={(caseRunItem && item._id === caseRunItem._id) ? "case-run__row case-run__row--active" : "case-run__row"}>
            {(item && item.pass) && <img src={CheckIcon} alt="Pass" />}
            {(item && item.fail) && <img src={FailIcon} alt="Fail" />}
            {(item && item.block) && <img src={BlockIcon} alt="Block" />}
            {(item && item.query) && <img src={QueryIcon} alt="Query" />}
            {(item && item.exclude) && <img src={Exclude} alt="Exclude" />}
        </div>
    )
}
