import React, { useContext } from 'react'
import { Link } from 'react-router-dom'

import WebizpadLogo from '../../../assets/icons/webizpad/a70f5e31-4009-423e-a411-1fe8e501085b_200x200.png';
import SettingsDropdown from '../../ReusableComponents/SettingsDropdown/index';
import { UserContext } from './../../../contexts/UserContext';

const CaseHeader = React.memo(() => {
    const { user } = useContext(UserContext)
    return (
        <div className='cases__header'>
            <div className="cases__header-left">
                <div className="website-logo">
                    <Link to="/projects">
                        <img src={WebizpadLogo} alt="" />
                    </Link>
                </div>


            </div>

            <div className="cases__header-right">
                <div className="cases__btns">
                    <button>Script</button>
                    <button>Edit</button>
                    <button>Reporting</button>
                </div>
                <div className="user-info">
                    {user && user._username}
                </div>
                <SettingsDropdown />
            </div>
        </div>
    )
})

export default CaseHeader;