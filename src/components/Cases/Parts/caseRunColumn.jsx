import React, { useEffect, useState, useRef } from 'react';

import { getStepRunItems, getStepRunItem } from '../../../services/caseRunItemsServices';

import CheckIcon from '../../../assets/icons/check.png';
import FailIcon from '../../../assets/icons/fail.png';
import QueryIcon from '../../../assets/icons/questionMark.png';
import BlockIcon from '../../../assets/icons/block.png';
import Exclude from '../../../assets/icons/minus-black.png';


export default function CaseRunColumn({ item, caseSteps, setCaseRun, setParentRun, handleRunColumnClick, setCaseRunItem, setCaseRunItemActive, caseRunItem }) {
    const [stepRunItems, setStepRunItems] = useState(null);

    useEffect(() => {
        handleGetStepRunItems();
    }, [])

    useEffect(() => {
        handleGetStepRunItems();
    }, [caseSteps, caseRunItem])

    const handleGetStepRunItems = async () => {
        const result = await getStepRunItems(item._id);
        setStepRunItems(result.data);
    }

    const handleSetParentRun = () => {
        setParentRun(item);
    }

    const handleCompletedOverlayClick = () => {
        setParentRun(item);
        setCaseRunItemActive(true);
    }

    return (
        <div className='case-run__column'>
            <div className="run-column__header" onClick={() => handleRunColumnClick(item)}>
                <div className="run-column__id">
                    #{item.queueNumber}
                </div>
                <div className="run-column__date">
                    {item.date}
                </div>
            </div>
            <div className="run-column__body">
                {stepRunItems && stepRunItems.map((c) => {
                    return <RunColumnRow key={c._id} item={c} caseRun={item} setParentRun={setParentRun} handleSetParentRun={handleSetParentRun} caseRunItem={caseRunItem} setCaseRunItem={setCaseRunItem} setCaseRunItemActive={setCaseRunItemActive} />
                })}
                {!item.inProgress && <div className="run-column__body--completed" onClick={() => handleCompletedOverlayClick()}>
                    <div>Completed</div>
                </div>}
            </div>
        </div>
    )
}

function RunColumnRow({ item, handleSetParentRun, caseRun, setParentRun, caseRunItem, setCaseRunItem, setCaseRunItemActive }) {

    const handleStepRunItemClick = () => {
        setCaseRunItem(item);
        setParentRun(caseRun);
        setCaseRunItemActive(true);
        handleSetParentRun();
    }
    return (
        <div className={(caseRunItem && item._id === caseRunItem._id) ? "case-run__row case-run__row--active" : "case-run__row"} onClick={() => handleStepRunItemClick()}>
            {(item && item.pass) && <img src={CheckIcon} alt="Pass" />}
            {(item && item.fail) && <img src={FailIcon} alt="Fail" />}
            {(item && item.block) && <img src={BlockIcon} alt="Block" />}
            {(item && item.query) && <img src={QueryIcon} alt="Query" />}
            {(item && item.exclude) && <img src={Exclude} alt="Exclude" />}
        </div>
    )
}
