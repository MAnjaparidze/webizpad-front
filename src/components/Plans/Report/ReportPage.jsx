import React, { useState, useEffect } from 'react'

import ProgressBar from '../../ReusableComponents/ProgressBar/index';
import ReportCaseItem from '../Parts/planReportCaseItem';
import '../Styles/main.css';

import { getCases } from '../../../services/casesService';
import { getPlan } from '../../../services/plansService';
import CaseHeader from './../../Cases/Parts/caseHeader';

export default function ReportPage() {
    const [planItem, setPlanItem] = useState(null);
    const [caseItems, setCaseItems] = useState(null);

    const handleGetPlanItem = async () => {
        let url = window.location.href;
        let start = url.indexOf("report") + 7;
        let end = url.length;
        let itemId = url.substr(start, end)
        let result = await getPlan(itemId);
        setPlanItem(result.data);
    }

    useEffect(() => {
        handleGetPlanItem();
    }, [])

    useEffect(() => {
        if(planItem) handleGetCases(planItem._id)
    },[planItem])

    const handleGetCases = async (id) => {
        const { data } = await getCases({ parentID: id });
        setCaseItems(data);
    }

    return (
        <div className='plan-report-page'>
            <CaseHeader />
            <div className="plans-page__header">
                <span className='cases-body-header__name'>{`${planItem && planItem.name}`}</span>
                {planItem && <ProgressBar item={planItem} />}
            </div>

            <div className="plans-page__body">
                {caseItems && caseItems.map(item => {
                    return <ReportCaseItem key={item._id} caseItem={item} />
                })}
            </div>
        </div>
    )
}
