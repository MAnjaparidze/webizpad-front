import React from 'react'

import '../Styles/main.css';

export default function PlanProgressBar({ plan, project }) {
    return (
        <div className='plan-progress-bar'>
            <div className='item-name__wrapper'>
                <span className='item-name'>{project && project.name} {">"} {plan && plan.name}</span>
            </div>
            <div className="item-progress__wrapper">
                {plan && <div className="item-progress-naming">
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--green'></div>
                        {plan.passNum}
                    </div>
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--red'></div>
                        {plan.failNum}
                    </div>
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--black'></div>
                        {plan.blockedNum}
                    </div>
                    <div className='item-naming__wrapper'>{plan.passNum} / {plan.testNum}</div>
                    {/* <div className='item-naming__wrapper'>{plan.passNum / plan.textNum * 100}%</div> */}
                    <div className='item-naming__wrapper'>0%</div>
                </div>}
                <div className='item-progress-bar'>
                    <div className="item-progress-bar--done"></div>
                </div>
            </div>
        </div>
    )
}
