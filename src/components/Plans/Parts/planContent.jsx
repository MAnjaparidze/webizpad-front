import React, { useEffect, useState, useRef } from 'react'
import { useHistory } from 'react-router-dom';

import InputItem from '../../ReusableComponents/Input/Input';
import ArrowDownIcon from '../../../assets/icons/arrow-down.png';

import ProjectTableItem from '../../ReusableComponents/Project-Table-Item/index';

export default function PlanContent({ currentProject, currentPlan, handleAddCase, currentCases }) {
    const [isCaseActive, setCaseActive] = useState(false);
    const [isPlanActive, setPlanActive] = useState(false);
    const [isFormActive, setFormActive] = useState(false);
    const [itemName, setItemName] = useState(null);
    const [type, setType] = useState("");

    const history = useHistory();

    const handleCaseClick = () => {
        setCaseActive(!isCaseActive);
        setPlanActive(false);
    }
    const handlePlanClick = () => {
        setPlanActive(!isPlanActive);
        setCaseActive(false);
    }

    const handleAddBtnClick = (type) => {
        setType(type);
        setFormActive(true);
    }

    const handleViewReportClick = () => {
        history.push(`/plans/report/${currentPlan._id}`);
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();
        let item = {
            name: itemName,
            parentID: currentPlan._id,
            testNum: 0,
            passNum: 0,
            failNum: 0,
            queryNum: 0,
            blockedNum: 0,
            excludeNum: 0
        };
        if (type === "Case") {
            handleAddCase(item);
        } else if (type === 'Plan') {
            // handleAddPlan(item);
        }
        setFormActive(false);
    }

    const handleChange = (e) => {
        e.preventDefault();
        setItemName(e.target.value)
    }

    const handleFormCancel = (e) => {
        e.preventDefault();
        setFormActive(false);
    }

    function useOutsideClick(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setCaseActive(false);
                    setPlanActive(false);
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    function ContentButtons() {
        const wrapperRef = useRef(null);
        useOutsideClick(wrapperRef);

        return (
            <div ref={wrapperRef} className="content__buttons-container">
                <div className="new-case__dropdown">
                    <button className={"drop__btn"} onClick={() => handleCaseClick()}>New Case <img src={ArrowDownIcon} alt="" /></button>
                    <div className={isCaseActive ? "dropdown-content dropdown-content--active" : "dropdown-content"}>
                        <button onClick={() => handleAddBtnClick("Case")}>+ New Case</button>
                        <button>- Dummy Button</button>
                    </div>
                </div>
                <div className="new-plan__dropdown">
                    <button className={"drop__btn"} onClick={() => handlePlanClick()}>New Plan <img src={ArrowDownIcon} alt="" /></button>
                    <div className={isPlanActive ? "dropdown-content dropdown-content--active" : "dropdown-content"}>
                        <button onClick={() => handleAddBtnClick("Plan")}>+ New Plan</button>
                        <button>Test Button 2</button>
                    </div>
                </div>
                <div className="new-plan__dropdown">
                    <button className={"drop__btn btn--grey"} onClick={() => handleViewReportClick()}>View Report</button>
                </div>
            </div>
        );
    }



    return (
        <div className='content-wrapper plan-content'>
            {ContentButtons()}
            <div className="plan-item__wrapper">
                <ProjectTableItem project={currentProject} item={currentPlan} isDisabled={true} />

                <div className="plan-item-cases">
                    {currentCases && currentCases.map(item => {
                        return <ProjectTableItem key={item._id} project={currentProject} linkTo={"cases"} item={item} />
                    })}
                </div>
            </div>

            {isFormActive && <form className="submit__form" onSubmit={e => handleFormSubmit(e)}>
                <h3>Create New Item</h3>
                <InputItem type="text" name="caseName" label={`${type} Name`} onChange={handleChange} />
                <div className="submit__form-btns">
                    <button className='form-cancel__btn' type="button" onClick={e => handleFormCancel(e)}>Cancel</button>
                    <button className='form-submit__btn' type="submit">Submit</button>
                </div>
            </form>}
        </div>
    )
}
