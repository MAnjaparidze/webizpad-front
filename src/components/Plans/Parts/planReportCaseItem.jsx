import React, { useEffect, useState } from 'react'


import { getCaseRuns } from '../../../services/caseRunsServices';

import ReportPageLogs from '../../ReusableComponents/ReportPageLogs/index';
import ProgressBar from '../../ReusableComponents/ProgressBar/index';

export default function PlanReportCaseItem({ caseItem }) {
    const [caseRuns, setCaseRuns] = useState(null);

    useEffect(() => {
        handleGetCaseRuns();
    }, [])

    const handleGetCaseRuns = async() => {
        let result = await getCaseRuns(caseItem._id);
        setCaseRuns(result.data);
    }

    return (
        <div className='plan-report-case-item'>
            <div className="plan-report-case__header">
                <span className="plan-report-case__name">
                    {caseItem.name}
                </span>
                <ProgressBar item={caseItem} />
            </div>
            <div className="plan-report-case-runs">
                {caseRuns && caseRuns.map(runItem => {
                    return <ReportPageLogs item={runItem} comments={false} />
                })}
            </div>
        </div>
    )
}
