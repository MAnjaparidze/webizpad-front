import React from 'react'

import ProgressBar from '../../ReusableComponents/ProgressBar/Styles';

export default function reportPageCase() {
    return (
        <div className='report-page-case'>
            <div className="report-page-case--header">
                <span className="report-page-case--name">

                </span>
                <ProgressBar />
            </div>
        </div>
    )
}
