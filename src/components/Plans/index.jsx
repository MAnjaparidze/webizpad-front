import React, { useEffect, useState, useContext, useRef } from 'react';

import { getProjects, getProject, addProject } from '../../services/projectsService';
import { getPlans, getPlan, addPlan } from '../../services/plansService';
import { getCases, addCase } from '../../services/casesService';

import NavBar from '../ReusableComponents/NavBar/index';
import Header from '../ReusableComponents/Header/index';
import PlanProgressBar from './Parts/planProgressBar';
import PlanContent from './Parts/planContent';
import { UserContext } from './../../contexts/UserContext';

export default function Index(props) {
    const [projects, setProjects] = useState();
    const [currentProject, setCurrentProject] = useState(null);
    const [currentPlan, setCurrentPlan] = useState(null);
    const [currentCases, setCurrentCases] = useState(null);

    const { user } = useContext(UserContext);
    const { location } = props;

    useEffect(() => {
        handleURL();
    }, [])

    useEffect(() => {
        if (user) {
            handleGetProjects(user);
        }
    }, [user])

    useEffect(() => {
        if(currentPlan) handleGetCases(currentPlan._id)
    },[currentPlan])

    const handleURL = async () => {
        let url = window.location.href;
        let start = url.indexOf("/plans/") + 7;
        let projectID = url.substr(31, 24); 
        let planID = url.substr(start, url.length)
        await handleGetProject(projectID);
        await handleGetPlan(planID);
    }

    const handleGetProjects = async (userObj) => {
        const { data } = await getProjects(userObj);
        setProjects(data);
    }

    const handleGetProject = async (id) => {
        const { data } = await getProject(id);
        setCurrentProject(data);
    }

    const handleGetPlan = async (id) => {
        const { data } = await getPlan(id);
        setCurrentPlan(data);
    }

    const handleGetCases = async (id) => {
        const { data } = await getCases({ parentID: id });
        setCurrentCases(data);
    }

    const handleAddCase = async (item) => {
        await addCase(item);
        await handleGetCases(currentPlan._id);
    }

    return (
        <div className='container'>
            <NavBar
                user={user}
                projects={projects}
                currentProject={currentProject}
                setCurrentProject={setCurrentProject}
                handleGetProjects={handleGetProjects}
                addProject={addProject}
            />
            <div className="content-container">
                <Header currentProject={currentProject} user={user} />
                <PlanProgressBar plan={currentPlan} project={currentProject} />
                {currentPlan && <PlanContent
                    currentProject={currentProject}
                    currentPlan={currentPlan}
                    handleAddCase={handleAddCase}
                    currentCases={currentCases}
                />}

            </div>
        </div>
    )
}
