import React, { Component, useContext, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom';
import { UserContext } from '../../contexts/UserContext';
import Joi from 'joi-browser';
import jwtDecode from 'jwt-decode';

import Input from '../ReusableComponents/Input/Input';
import { login } from '../../services/authService';

import LandingPageIMG from '../../assets/img/landing-page-form-image-cropped.png';

export default class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: { email: "", password: "" },
            errors: {},
        }
    }

    schema = {
        email: Joi.string().required().label("Email"),
        password: Joi.string().required().label("Password")
    }

    validate = () => {
        const { account } = this.state;
        const options = { abortEarly: false };

        const result = Joi.validate(account, this.schema, options);

        if (!result.error) return null;

        const errors = {};
        for (let item of result.error.details)
            errors[item.path[0]] = item.message;

        return errors;
    }

    handleChange = ({ currentTarget: input }) => {
        const account = { ...this.state.account };
        account[input.name] = input.value;

        this.setState({ account });
    }

    doSubmit = async (e) => {
        e.preventDefault();
        this.setState({ errors: {} })
        try {
            const { account } = this.state;
            const { data: jwt } = await login(account.email, account.password);
            localStorage.setItem('token', jwt);
            const userObj = jwtDecode(jwt);
            console.log(userObj);
            if(userObj._isActivated) {
                this.props.history.push('/projects');
            } else {
                this.props.history.push("/authentication/not-active")
            }
        } catch (ex) {
            if (ex.response && ex.response.status === 400) {
                const errors = { ...this.state.errors };
                errors.email = ex.response.data;
                this.setState({ errors });
            }
        }
    }



    render() {
        const { account, errors } = this.state;
        return (
            <div className='container'>
                <div className="landing-form-wrapper">
                    <Login
                        account={account}
                        errors={errors}
                        doSubmit={this.doSubmit}
                        handleChange={this.handleChange}
                    />
                    <div className="landing-form-image-wrapper">
                        <img src={LandingPageIMG} alt="" className="landing-form-image" />
                    </div>

                </div>
                <div className='landing-container-overlay'>
                </div>
            </div>
        )
    }
}

function Login({ account, errors, doSubmit, handleChange }) {

    const { user } = useContext(UserContext)

    const history = useHistory();

    // useEffect(() => {
    //     if (user) {
    //         history.push("/projects")
    //     }
    // }, [])

    return (
        <div className='form__container'>
            <h3>Get started with the 30-day free</h3>
            <form onSubmit={e => doSubmit(e)}>

                <Input name='email' value={account.email} label="Email" error={errors.email} type='text' onChange={handleChange} />
                <Input name='password' value={account.password} label="Password" error={errors.password} type='password' onChange={handleChange} />

                <button type="submit">Sign In</button>
                <Link to="/forgot-password">Forgot your password?</Link>
                <Link to="/register">Don't Have an Account?</Link>
            </form>
        </div>
    )
}
