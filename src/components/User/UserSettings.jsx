import React, { useState, useEffect, useContext } from 'react';

import Joi from 'joi-browser';

import NavBar from '../ReusableComponents/NavBar/index';
import Header from '../ReusableComponents/Header';
import { getProjects, addProject } from './../../services/projectsService';
import { updateUserInfo } from './../../services/userService';

import { UserContext } from './../../contexts/UserContext';
import Input from './../ReusableComponents/Input/Input';

import UserIcoGreen from '../../assets/icons/user/user-green.png';

import './Styles/main.css';

export default function UserSettings() {
    const [projects, setProjects] = useState();
    const [currentProject, setCurrentProject] = useState(null);

    const { user, handleUser } = useContext(UserContext);

    useEffect(() => {
        if (user) {
            handleGetProjects(user);
        }
    }, [user])

    const handleGetProjects = async (userObj) => {
        const { data } = await getProjects(userObj);
        setProjects(data);
    }

    return (
        <div className='container'>
            <NavBar
                projects={projects}
                user={user}
                currentProject={currentProject}
                setCurrentProject={setCurrentProject}
                handleGetProjects={handleGetProjects}
                addProject={addProject}
            />
            <div className="content-container">
                <Header currentProject={currentProject} user={user} />
                <div className="user-settings-wrapper">
                    <UserSettingsForm user={user} handleUser={handleUser} />
                </div>
            </div>
        </div>
    )
}

function UserSettingsForm({ user, handleUser }) {
    const [username, setUser_Name] = useState("");
    const [user_email, setUser_Email] = useState("");
    const [user_password, setUser_Password] = useState("");
    const [errors, setErrors] = useState([]);
    const [userRole, setUserRole] = useState(null);
    const [changeUserActive, toggleChangeUserActive] = useState(false);

    const schema = {
        username: Joi.string().regex(/^[a-zA-Z]+$/).min(3).max(100).required().error(() => { return { message: "Username should contain only letters and be more than 3 letters" } }),
        user_email: Joi.string().min(8)
            .email({ minDomainSegments: 2 }).required().label("Email"),
        user_password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,64})/).error(() => { return { message: 'The password must be 8 to 64 characters long, contain at least one Upper & Lower Case letters, one number and one special character.' } }),
        // repeat_password: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } }).error(() => { return { message: "Dedis tyvna " } }),
    }

    useEffect(() => {
        if (user) {
            setUser_Name(user._username);
            setUser_Email(user._email);
            getUserRole(user._role);
        }
    }, [user])

    const validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const demoSchema = { [name]: schema[name] };
        const { error } = Joi.validate(obj, demoSchema);
        return error ? error.details[0].message : null;
    }

    const validate = () => {
        const account = {
            username: username,
            user_email: user_email,
            user_password: user_password
        }
        const options = { abortEarly: false };

        const result = Joi.validate(account, schema, options);

        if (!result.error) return null;

        const errors = {};
        for (let item of result.error.details)
            errors[item.path[0]] = item.message;

        return errors;
    }

    const handleChange = ({ currentTarget: input }) => {
        let errObj = { ...errors };
        const errorMessage = validateProperty(input);

        if (errorMessage) {
            errObj[input.name] = errorMessage;
        } else {
            delete errObj[input.name];
        }
        switch (input.name) {
            case ("username"):
                setUser_Name(input.value);
                break;
            case ("user_email"):
                setUser_Email(input.value);
                break;
            case ("user_password"):
                setUser_Password(input.value);
                break;
            default:
                break;
        }
        setErrors(errObj);
    }

    const getUserRole = (role_id) => {
        let roleArr = ["Owner", "Admin", "Normal"];
        let roleObj = { id: role_id, role: roleArr[role_id] }
        setUserRole(roleObj);
    }

    useEffect(() => {
        console.log(userRole);
    },[userRole])

    const handleSubmit = async (e) => {
        e.preventDefault();
        const errs = validate();
        if (errs) return setErrors(errs);

        setErrors([]);
        let userObj = {
            _id: user._id,
            username: username,
            email: user_email,
            role: userRole.id,
            password: user_password
        }

        let result = await updateUserInfo(userObj);
        localStorage.setItem('token', result.data);
        handleUser();
    }


    return (
        <form onSubmit={(e) => handleSubmit(e)} className="user-settings-form">
            <div className="user-settings-role">
                <label for='user-role'>User Type</label>
                <div className='user-role-wrapper'>
                    <img src={UserIcoGreen} alt="" />
                    <div id='user-role'>
                        {userRole && userRole.role}
                    </div>
                    {(user && (user._role === 1 || user._role === 0)) && <button type="button" onClick={() => toggleChangeUserActive(true)} className='user-role-change'>Change Type</button>}
                </div>
            </div>
            <Input type="text" name="username" label={`Name`} value={username} error={errors.username && errors.username} onChange={handleChange} />
            <Input type="text" name="user_email" label={`Email`} value={user_email} error={errors.user_email && errors.user_email} onChange={handleChange} />
            <Input type="password" name="user_password" label={`Password`} value={user_password} error={errors.user_password && errors.user_password} onChange={handleChange} />
            <div className="user-settings-form-btns">
                <button type="submit" className='save-changes'>Save Changes</button>
                <button type='reset' className='cancel-changes'>Cancel</button>
            </div>
            {changeUserActive && <ChangeUserTypeForm user={user} getUserRole={getUserRole} toggleChangeUserActive={toggleChangeUserActive} />}

        </form>

    )
}

function ChangeUserTypeForm({ user, getUserRole, toggleChangeUserActive }) {

    const handleRoleChoose = (role_id) => {
        getUserRole(role_id);
        toggleChangeUserActive(false);
    }

    return (
        <>
            <div className="user-role-change-form">
                <div className="user-role-change-header">
                    <div className="user-role-change-header-text">
                        SELECT A NEW USER LEVEL
                </div>
                    <button type="button" onClick={() => toggleChangeUserActive(false)}>Cancel</button>
                </div>
                <div className="user-role-type">
                    <button className="choose-role" onClick={() => { handleRoleChoose(2);  }}>Select</button>
                    <div className="role-icon">
                        <img src={UserIcoGreen} alt="" />
                        <span>Normal</span>
                    </div>
                    <div className="role-description">
                        Normal users can create and edit scripts, but can't delete scripts or add new users.
                </div>
                </div>
                <div className="user-role-type">
                    <button className="choose-role" onClick={() => handleRoleChoose(1)}>Select</button>
                    <div className="role-icon">
                        <img src={UserIcoGreen} alt="" />
                        <span>Admin</span>
                    </div>
                    <div className="role-description">
                        Admins can edit any settings, except those relating to account onwership. Admin users can add new users and have permission to delete.
                    </div>
                </div>
                <div className="user-role-type">
                    <button className="choose-role" onClick={() => handleRoleChoose(0)}>Select</button>
                    <div className="role-icon">
                        <img src={UserIcoGreen} alt="" />
                        <span>Owner</span>
                    </div>
                    <div className="role-description">
                        Account owners can edit any settings, including payment details. There must be at least one user set as the account owner. Only account owners can choose and add new owners.
                    </div>
                </div>
            </div>
            <div className="grey-overlay"></div>
        </>
    )
}