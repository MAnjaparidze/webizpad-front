import React, { useEffect, useState, useContext } from 'react'

import NavBar from '../ReusableComponents/NavBar/index';
import Header from '../ReusableComponents/Header/index';

import { getProjects, addProject } from '../../services/projectsService';
import { getTeam, addMember } from '../../services/teamService';

import { UserContext } from './../../contexts/UserContext';

import SettingsIco from '../../assets/icons/settings.png';
import Input from './../ReusableComponents/Input/Input';

export default function ManageUsers() {
    const [projects, setProjects] = useState();

    const { user, handleUser } = useContext(UserContext);

    useEffect(() => {
        handleUser();
    }, [])

    useEffect(() => {
        if (user) {
            handleGetProjects(user)
        }
    }, [user])

    const handleGetProjects = async (userObj) => {
        const { data } = await getProjects(userObj);
        setProjects(data);
    }

    return (
        <div className='container'>
            <NavBar
                user={user}
                projects={projects}
                handleGetProjects={handleGetProjects}
                addProject={addProject}
            />
            <div className="content-container">
                <Header
                    user={user}
                />
                {user && <ManageUsersContent user={user} />}
            </div>
        </div>
    )
}

function ManageUsersContent({ user }) {
    const [teamMembers, setTeamMembers] = useState(null);
    const [addUserActive, toggleAddUserActive] = useState(false);
    const [email, setEmail] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [errors, setErrors] = useState([]);

    useEffect(() => {
        handleGetTeamMembers();
    }, [user])

    const handleGetTeamMembers = async () => {
        let result = await getTeam(user._team);
        console.log(result);
        setTeamMembers(result.data.members);
    }

    const handleChange = () => {

    }

    return (
        <div className='manage-users-page'>
            <div className="manage-users-main-btns">
                <button className='manage-users-new' onClick={() => toggleAddUserActive(true)}>+ New User</button>
                <div className="manage-users-sorting-btns">
                    <span>Sort By</span>
                    <button className='manage-users-firstname'>First Name</button>
                    <button className='manage-users-lastname'>Last Name</button>
                    <button className='manage-users-email'>Email</button>
                </div>
            </div>

            <div className="manage-users-content">
                {teamMembers && teamMembers.map((item, i) => {
                    return <UserCard key={i} user={item} />
                })}
            </div>

            {addUserActive && <div className='add-user-modal'>
                <Input name='email' value={email} label="Email" error={errors.email} type='text' onChange={handleChange} />
                <Input name='firstname' value={firstname} label="Firstname" error={errors.firstname} type='text' onChange={handleChange} />
                <Input name='lastname' value={lastname} label="Lastname" error={errors.lastname} type='text' onChange={handleChange} />
                <div className="user_role-selector">
                    <label htmlFor="user_role">Role</label>
                    <select name="user_role" id="user_role">
                        <option value={0}>Owner</option>
                        <option value={1}>Admin</option>
                        <option value={2}>User</option>
                    </select>
                </div>
                <div className="add-user-buttons">
                    <button className='invite-btn'>Invite</button>
                    <button className='cancel-btn' onClick={() => toggleAddUserActive(false)}>Cancel</button>
                </div>
            </div>}
        </div>
    )
}

function UserCard({ user }) {
    const [dropdownActive, toggleDropdownActive] = useState(false);

    const handleUserRole = () => {
        let roleArr = ["Owner", "Admin", "User"];

        return roleArr[user.role];
    }

    return (
        <div className="user-card-wrapper">
            <div className="user-card-header">
                <span className="user-card-name">
                    {`${user.firstname} ${user.lastname}`}
                </span>
                <div className="user-card-settings">
                    <img src={SettingsIco} alt="" onClick={() => { toggleDropdownActive(!dropdownActive) }} />
                    {dropdownActive && <div className="user-card-settings-drop">
                        <span className="user-settings-drop-item">Edit Details</span>
                        <span className="user-settings-drop-item">Send Password Reset Email</span>
                        <span className="user-settings-drop-item">Delete</span>
                    </div>}
                </div>
            </div>
            <div className="user-card-content">
                <div className="user-role">
                    {handleUserRole()}
                </div>
                <div className="user-email">
                    {user.email}
                </div>
                <div className="user-joined">
                    Joined {user.date}
                </div>
            </div>
        </div>
    )
}
