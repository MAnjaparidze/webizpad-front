import React, { useContext } from 'react'
import { UserContext } from '../../contexts/UserContext';

import './Styles/main.css';

export default function FreeSubscription() {

    const { user } = useContext(UserContext)

    return (
        <div className="container">
            <div className='free-sub-container'>
                <h1 className="free-sub-header">Free Trial</h1>
                <p>Hello {user && user._username}</p>
                <p>Thank you for registering on Webizpad. We hope you have been enjoying your free trial.</p>
                <p className='free-sub-timeleft'>Your trial has 14 days left.</p>
                <p>We would love to keep you as a customer, and there is still time to complete your sbscription!</p>
                <p>If you have any questions and feedback please let us know</p>
                <p className='free-sub-regards'>Best Regards, <br /> Webizpad Team</p>
                <div className="free-sub-btns">
                    <button className="free-sub-continue">Continue</button>
                    <button className="free-sub-subscribe">Go to Subscribe</button>
                </div>
            </div>
            <div className='landing-container-overlay'>
            </div>
        </div>
    )
}
