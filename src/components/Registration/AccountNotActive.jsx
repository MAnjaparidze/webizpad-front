import React from 'react';
import { Link } from 'react-router-dom';

export default function AccountNotActive() {
    return (
        <div className='container'>
            <div className="status-confirmation">
                <div>Your Account is Not Activated</div>
                <div>Please check your email for activation link</div>
                <div className='status-confirm-login'><Link to="/" className='status-confirm-login-link'>Login</Link></div>
            </div>
            <div className='landing-container-overlay'>
            </div>
        </div>
    )
}
