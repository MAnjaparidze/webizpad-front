import React, { Component } from 'react'
import Joi from 'joi-browser';

import { Link } from 'react-router-dom'

import Input from '../ReusableComponents/Input/Input';
import './Styles/registerForm.css';

import ArrowGreen from '../../assets/icons/arrow-right-green.png';

import LandingPageIMG from '../../assets/img/landing-page-form-image-cropped.png';
import * as userService from '../../services/userService';

import { createTeam } from '../../services/teamService';

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            account: { username: "", email: "", password: "", teamname: "", firstname: "", lastname: "", isTeamMember: false },
            errors: {},
            isFirstPage: true,
            registerSuccess: false,
        }
    }

    schema = {
        username: Joi.string().regex(/^[a-zA-Z\_]+$/).min(3).max(100).required().error(() => { return { message: "Username should contain only letters and be more than 3 letters" } }),
        firstname: Joi.string().regex(/^[a-zA-Z \-]+$/).required().min(3).max(250),
        lastname: Joi.string().regex(/^[a-zA-Z \-]+$/).required().min(3).max(250),
        email: Joi.string().min(8)
            .email({ minDomainSegments: 2 }).required().label("Email"),
        password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,64})/).error(() => { return { message: 'The password must be 8 to 64 characters long, contain at least one Upper & Lower Case letters, one number and one special character.' } }),
        repeat_password: Joi.any().valid(Joi.ref('password')).required().error(() => { return { message: "The passwords don't match." } }),
        teamname: Joi.string().regex(/^[a-zA-Z \-]+$/).required().min(3).max(100).error(() => { return { message: "Team Name should be only text and max 100 characters long" } }),
        isTeamMember: Joi.boolean().required()
    }

    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema);
        return error ? error.details[0].message : null;
    }

    validate = () => {
        const { account } = this.state;
        const options = { abortEarly: false };

        const result = Joi.validate(account, this.schema, options);

        if (!result.error) return null;

        const errors = {};
        for (let item of result.error.details)
            errors[item.path[0]] = item.message;

        return errors;
    }

    handleChange = ({ currentTarget: input }) => {
        const errors = { ...this.state.errors };
        const options = { abortEarly: false };
        let errorMessage = null;

        const schema = {
            password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,64})/).error(() => { return { message: 'The password must be 8 to 64 characters long, contain at least one Upper & Lower Case letters, one number and one special character.' } }),
        repeat_password: Joi.any().valid(Joi.ref('password')).required().error(() => { return { message: "The passwords don't match." } }),
        }

        if(input.name === 'repeat_password') {
            const { error } = Joi.validate({ password: this.state.account.password, repeat_password: input.value }, schema, options);
            if (error) {
                if (error.details.length === 2) {
                    errorMessage = error.details[1].message;
                } else {
                    errorMessage = error.details[0].message;
                }
            }
        } else {
            errorMessage = this.validateProperty(input);
        }

        if (errorMessage) errors[input.name] = errorMessage;
        else delete errors[input.name];

        const account = { ...this.state.account };
        account[input.name] = input.value;

        this.setState({ account, errors });
    }

    handleSubmit = e => {
        e.preventDefault();

        const errors = this.validate();

        this.setState({ errors: errors || {} })
        if (errors) return console.log("Something got wrong", errors);
        this.doSubmit();
    }

    doSubmit = async () => {
        console.log(this.state.account);
        try {
            let response = await userService.register(this.state.account);
            if (response.status === 200) this.setState({ registerSuccess: true });
        } catch (ex) {
            if (ex.response && ex.response.status === 400) {
                const errors = { ...this.state.errors }
                errors.email = ex.response.data;
                this.setState({ errors });
            }
        }
    }

    render() {
        const { account, errors, isFirstPage } = this.state;
        return (
            <div className='container'>
                <div className="landing-form-wrapper">
                    {!this.state.registerSuccess ? <div className='form__container'>
                        <h3>Get started with the 30-day free</h3>
                        <form onSubmit={e => this.handleSubmit(e)}>
                            {isFirstPage && <div>
                                <Input name='username' value={account.username} label="Username" error={errors.username} type='text' onChange={this.handleChange} />
                                <Input name='email' value={account.email} label="Email" error={errors.email} type='text' onChange={this.handleChange} />
                                <Input name='password' value={account.password} label="Password" error={errors.password} type='password' onChange={this.handleChange} />
                                <Input name='repeat_password' label="Repeat Password" type='password' error={errors.repeat_password} onChange={this.handleChange} />
                                <div className="reg-page-next" onClick={() => { this.setState({ isFirstPage: false }) }}>
                                    <span>Next</span>
                                    <img src={ArrowGreen} alt="" />
                                </div>
                            </div>}
                            {!isFirstPage && <div>
                                <Input name='firstname' value={account.firstname} label="First Name" error={errors.firstname} type='text' onChange={this.handleChange} />
                                <Input name='lastname' value={account.lastname} label="Last Name" error={errors.lastname} type='text' onChange={this.handleChange} />
                                {/* <div className="team-status">
                                    <label htmlFor="team-status">Team Status</label>
                                </div> */}
                                <Input name='teamname' value={account.teamname} label="Team Name" error={errors.teamname} type='text' onChange={this.handleChange} />
                                <div className="reg-page-prev" onClick={() => this.setState({ isFirstPage: true })}>
                                    <img src={ArrowGreen} alt="" />
                                    <span>Prev</span>
                                </div>
                            </div>}

                            {!isFirstPage && <button type='submit'>Start My Free Trial</button>}
                            <Link to="/">Already Registered? Sign In</Link>
                        </form>
                    </div> : <RegistrationSuccess />}
                    <div className="landing-form-image-wrapper">
                        <img src={LandingPageIMG} alt="" className="landing-form-image" />
                    </div>
                </div>
                <div className='landing-container-overlay'>
                </div>
            </div>
        )
    }
}



function RegistrationSuccess() {

    return (
        <div className="register-success">
            <h3>You have registered successfully!</h3>
            <h4>Please check activation link sent on your email</h4>
            <Link to="/">Sign In</Link>
        </div>
    )
}

export default Index;
