import React, { useEffect, useState } from 'react';

import { activateAccount } from '../../services/userService';
import { Link } from 'react-router-dom';

export default function ActivateAccount() {
    const [message, setMessage] = useState(null);
    const [errors, setErrors] = useState(null);
    const [isTokenValid, setIsTokenValid] = useState(null);

    useEffect(() => {
        handleGetUrl();
    }, [])

    const handleGetUrl = async () => {
        let url = window.location.href;
        let start = url.indexOf("activate/") + 9;
        let end = url.length;
        let token = url.substr(start, end)
        let result = await activateAccount(token);
        if (result.data.status === 200) {
            setIsTokenValid(true);
            setMessage(result.data.message)
        } else if (result.data.status === 401) {
            setErrors(result.data.error);
        }
    }

    return (
        <div className='container'>
            <div className="status-confirmation">
                <div>{message && message}</div>
                <div>{errors && errors}</div>
                <div className='status-confirm-login'>{isTokenValid && <Link to="/" className='status-confirm-login-link'>Login</Link>}</div>
            </div>
            <div className='landing-container-overlay'>
            </div>
        </div>
    )
}
