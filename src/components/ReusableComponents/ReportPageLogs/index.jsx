import React, { useEffect } from 'react'

import "./Styles/main.css";
import { block } from 'joi-browser';

export default function Index({ item }) {

    const doneStyle = {
        width: `${100 / item.totalNum * item.passNum}%`
    }

    const failStyle = {
        width: `${100 / item.totalNum * item.failNum}%`
    }

    const blockStyle = {
        width: `${100 / item.totalNum * item.blockNum}%`
    }

    const queryStyle = {
        width: `${100 / item.totalNum * item.queryNum}%`
    }

    useEffect(() => {
        // handleProgressBar();
    }, [])

    return (
        <div className="item-progress__wrapper">
            <div className="item-progress-naming">
                <div className="item-info">
                    <div className="item-number">
                        #{item && item.queueNumber}
                    </div>
                    <div className="grey-dot"></div>
                    <div className="item-alias">
                        {/* {item && item.testerAlias} */}
                        Anyone
                    </div>
                    <div className="grey-dot"></div>
                    <div className="item-date">
                        {item && item.date}
                    </div>
                </div>
                <div className="item-progress-naming-right">
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--green'></div>
                        {item && item.passNum}
                    </div>
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--red'></div>
                        {item && item.failNum}
                    </div>
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--black'></div>
                        {item && item.blockNum}
                    </div>
                    <div className='item-naming__wrapper'>
                        <div className='cases-item__indicator--purple'></div>
                        {item && item.queryNum}
                    </div>
                    <div className='item-naming__wrapper'>{item && item.passNum} / {item && item.totalNum}</div>
                    {/* <div className='item-naming__wrapper'>{item && item.passNum / item.textNum * 100}%</div> */}
                    <div className='item-naming__wrapper'>{item && (item.passNum / item.totalNum * 100).toFixed(0)}%</div>
                </div>
            </div>
            <div className='item-progress-bar'>
                <div style={doneStyle} className="item-progress-bar--done"></div>
                <div style={failStyle} className="item-progress-bar--fail"></div>
                <div style={blockStyle} className="item-progress-bar--block"></div>
                <div style={queryStyle} className="item-progress-bar--query"></div>
            </div>
            {item.comment && <div className="item-progress-comment">
                No Test Comments, Issues or Result Attachments
            </div>}
        </div>
    )
}
