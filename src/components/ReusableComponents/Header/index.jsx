import React, { useState } from 'react'


import SettingsDropdown from '../SettingsDropdown/index';
import SyncIcon from '../../../assets/icons/sync.png';

import './Styles/main.css';

export default function Index({ currentProject, user }) {

    return (
        <div className='header-container'>
            <h2>{currentProject && currentProject.name}</h2>
            <div className="header__user-info">
                <button className='sync-btn'>
                    <img src={SyncIcon} alt="Synchronization Button" />
                </button>
                <span>{user && user._username}</span>
                <div className='horizontal-line'></div>
                <span>{user && user._team}</span>
                <SettingsDropdown />
            </div>
        </div>
    )
}
