import React from 'react'
import Loader from 'react-loader-spinner'

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


export default function CustomLoader({ width, height, color, type, timeout }) {
    return (
        <Loader
            type={type}
            color={color}
            height={height}
            width={width}
            timeout={timeout}
        />
    )
}
