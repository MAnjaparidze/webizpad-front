import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import './Styles/main.css'

export default function Index({ item, linkTo, project, isDisabled }) {
    const [itemInfo, setItemInfo] = useState(null);

    const getItemInfo = () => {

    }

    return (
        <Link to={{
            pathname: `/projects/${project._id}/${linkTo}/${item._id}`,
            aboutProps: { item: item }
        }} className={`project-table__item ${isDisabled && "link-disabled"}`}>
            <div className='item-name__wrapper'>
                <span className='item-name'>{item.name}</span>
            </div>
            <div className="item-progress__wrapper">
                <div className="item-progress-naming">
                    <span>Pass - {item.passNum}</span>
                    <span>Fail - {item.failNum}</span>
                    <span>Blocked - {item.blockedNum}</span>
                    <span>Query - {item.queryNum}</span>
                    <span>Exclude - {item.excludeNum}</span>
                    <span>{item.passNum} / {item.testNum}</span>
                    {/* <span>{item.passNum / item.textNum * 100}%</span> */}
                    <span>0%</span>
                </div>
                <div className='item-progress-bar'>
                    <div className="item-progress-bar--done"></div>
                </div>
            </div>
        </Link>
    )
}
