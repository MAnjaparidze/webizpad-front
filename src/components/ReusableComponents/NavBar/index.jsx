import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import './Styles/main.css';

import ProjectsIcon from '../../../assets/icons/projects-full-orange.png';
import PlusIcon from '../../../assets/icons/plus-orange.png';
import MinusIcon from '../../../assets/icons/minus-orange.png';

import WebizpadIcon from '../../../assets/icons/webizpad/a70f5e31-4009-423e-a411-1fe8e501085b_200x200.png'

export default function Index({ user, projects, currentProject, handleGetProjects, addProject }) {
    const [newProjectName, setNewProjectName] = useState("");
    const [error, setError] = useState(null);

    const [addProjectActive, setProjectActive] = useState(false);

    useEffect(() => {
        if (addProjectActive) document.getElementsByClassName("add-project__input")[0].focus();
    }, [addProjectActive])

    const handleProjectSubmit = async (e) => {
        e.preventDefault();
        try {
            const data = {
                name: newProjectName,
                author: user._id
            }
            let result = await addProject(data);
            handleGetProjects(user);
            setNewProjectName("")
            setProjectActive(false);
        } catch (ex) { }
    }

    const handleChange = (e) => {
        let target = e.target.value;
        let err = validateInput(target);
        if (err) {
            setNewProjectName(target);
        }
    }

    const validateInput = async (input) => {
        if (input.length < 3) {
            await setError(null);
            setError("Project name must be at least 3 characters long");
            return false;
        } else if (input.length > 100) {
            await setError(null);
            setError("Project name must not exceed 100 characters");
            return false;
        } else {
            await setError(null);
            return true;
        }
    }

    return (
        <div className='navbar-container'>
            <div className='header-container'>
                <img src={WebizpadIcon} alt=""/>
            </div>

            <div className='btn-container'>
                <img src={ProjectsIcon} alt="Projects Icon" />
                <h2>Projects</h2>
                <button className='add-project-btn' onClick={() => { setProjectActive(!addProjectActive) }}><img src={addProjectActive ? MinusIcon : PlusIcon} alt="" /></button>
            </div>
            <form id='add-project__form' className="add-project__form" onSubmit={e => handleProjectSubmit(e)}>
                <input className={addProjectActive ? 'add-project__input add-project__input--active' : "add-project__input"} value={newProjectName} placeholder="Project Name" type="text" min={3} max={100} required={true} onChange={e => handleChange(e)} />
                {error && <span className='add-project__error'>* {error}</span>}
            </form>

            <nav className='navbar'>
                <ul className='nav-item-wrapper'>
                    {projects && projects.map(project => {
                        return (
                            <li key={project._id}>

                                <div
                                    className={currentProject && (project._id === currentProject.id) ? 'nav-item nav-item--active' : "nav-item"}
                                >
                                    <Link to={`/projects/project/${project._id}`}>
                                        <span>{project.name}</span>
                                    </Link>
                                    <div className={currentProject && (project._id === currentProject.id) ? "nav__indicator nav__indicator--active" : "nav__indicator"}></div>


                                </div>
                                {/* <div className='nav-item nav-bug-item'>
                                    <span>Bugs</span>
                                </div> */}

                            </li>

                        )
                    })}
                </ul>
            </nav>
        </div>
    )
}
