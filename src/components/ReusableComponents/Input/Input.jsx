import React from 'react';

import './Styles/main.css';

const Input = ({ value, name, label, type, error, onChange }) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <input
                value={value}
                onChange={onChange}
                id={name}
                name={name}
                type={type}
                className="form-control"
            />
            {/* {console.log(error, "Error")} */}
            <div className="error-alert">{error && error}</div>
        </div>
    )
}

export default Input;