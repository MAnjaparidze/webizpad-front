import React from 'react'

import "./Styles/main.css";

export default function index({ item }) {
    return (
        <div className="item-progress__wrapper">
            <div className="item-progress-naming">
                <div className='item-naming__wrapper'>
                    <div className='cases-item__indicator--green'></div>
                    {item && item.passNum}
                </div>
                <div className='item-naming__wrapper'>
                    <div className='cases-item__indicator--red'></div>
                    {item && item.failNum}
                </div>
                <div className='item-naming__wrapper'>
                    <div className='cases-item__indicator--black'></div>
                    {item && item.blockNum}
                </div>
                <div className='item-naming__wrapper'>
                    <div className='cases-item__indicator--purple'></div>
                    {item && item.queryNum}
                </div>
                <div className='item-naming__wrapper'>{item && item.passNum} / {item && item.totalNum}</div>
                {/* <div className='item-naming__wrapper'>{item && item.passNum / item.textNum * 100}%</div> */}
                <div className='item-naming__wrapper'>0%</div>
            </div>
            <div className='item-progress-bar'>
                <div className="item-progress-bar--done"></div>
            </div>
        </div>
    )
}
