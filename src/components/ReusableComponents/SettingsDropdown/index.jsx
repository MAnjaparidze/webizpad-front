import React, { useState, useEffect, useRef } from 'react';
import { useHistory, Link } from 'react-router-dom';

import SettingsIcon from '../../../assets/icons/settings.png'

import './Styles/main.css'

export default function Index() {
    const [isSettingsBool, setSettingsBool] = useState(false);


    const history = useHistory();

    const handleLogOut = () => {
        localStorage.removeItem('token');
        history.push('/');
    }

    function useOutsideClick(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    setSettingsBool(false);
                }
            }
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    const wrapperRef = useRef(null);
    useOutsideClick(wrapperRef);

    return (
        <div ref={wrapperRef} className="settings-menu">
            <button className='settings-btn' onClick={() => setSettingsBool(!isSettingsBool)}>
                <img src={SettingsIcon} alt="" />
            </button>
            {isSettingsBool &&
                <div className="settings-wrapper">
                    <div className="profile-btn">
                        <Link to='/user/settings' > Profile and Log In </Link>
                    </div>
                    <div className="manage-users-btn">
                        <Link to='/user/manage-users' > Manage Users </Link>
                    </div>
                    <button className="logout-btn" onClick={() => handleLogOut()}>
                        Log Out
                    </button>
                </div>}
        </div>
    );


}
