import jwtDecode from 'jwt-decode';

const getUser = (setUser, jwtDecode) => {
    const jwt = localStorage.getItem('token');
    const user = jwtDecode(jwt);
    setUser(user);
}