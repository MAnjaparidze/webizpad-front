import http from "./httpService";

const apiEndpoint = "/users";

export function register(user) {
    return http.post(apiEndpoint, {
        user: {
            username: user.username,
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            password: user.password,
            teamname: user.teamname,
            isActivated: false,
            role: 1
        },
        isTeamMember: user.isTeamMember
    })
}

export function activateAccount(token) {
    let url = apiEndpoint + "/activate-account";
    return http.post(url, { token: token });
}

export function updateUserInfo(userObj) {
    let url = apiEndpoint + "/update";
    return http.put(url, userObj);
}

export function forgotPassword(email) {
    let url = apiEndpoint + "/forgot-password";
    return http.put(url, { email: email });
}

export function resetPassword(token, password) {
    let url = apiEndpoint + "/reset-password";
    return http.put(url, { token: token, password: password })
}

export function verifyResetToken(token) {
    let url = apiEndpoint + "/verify-token";
    return http.put(url, { type: "reset", token: token })
}