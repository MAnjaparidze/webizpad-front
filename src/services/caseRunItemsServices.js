import http from './httpService';

const apiEndpoint = "/caseRunItems";

export async function getStepRunItems(caseRunID) {
    let caseRun = { id: caseRunID };
    return await http.post(apiEndpoint, caseRun);
}

export async function getStepRunItem(caseRunID) {
    let caseRunItem = { id: caseRunID };
    let url = apiEndpoint + "/get";
    return await http.post(url, caseRunItem)
}

export async function addStepRunItem(caseRunItem) {
    let url = apiEndpoint + "/add";
    return await http.post(url, caseRunItem).then(res => { return res.data });
}

export async function updateSetRunItem(caseRunItemObj) {
    let url = apiEndpoint + "/update"
    return await http.post(url, caseRunItemObj).then(res => { return res.data })
}