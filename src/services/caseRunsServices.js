import http from './httpService';

const apiEndpoint = "/caseRuns";

export async function getCaseRuns(caseID) {
    let caseItem = { id: caseID };
    return await http.post(apiEndpoint, caseItem);
}

export async function getCaseRun(caseRunID) {
    let caseRun = { id: caseRunID };
    let url = apiEndpoint + "/get";
    return await http.post(url, caseRun)
}

export async function addCaseRun(caseRun) {
    let url = apiEndpoint + "/add";
    return await http.post(url, caseRun).then(res => { return res.data });
}

export async function updateCaseRun(caseRunObj) {
    let url = apiEndpoint + "/update";
    return await http.post(url, caseRunObj).then(res => { return res.data })
}

export async function deleteCaseRun(id) {
    const url = apiEndpoint + "/delete";
    const caseRunObj = { id: id }
    return await http.post(url, caseRunObj);
}