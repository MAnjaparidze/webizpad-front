import http from './httpService';

const apiEndpoint = "/cases";

export async function getCases(projectID) {
    return await http.post(apiEndpoint, projectID) ;
}

export async function getCase(caseID) {
    let caseItem = { id: caseID}
    let url = apiEndpoint + "/get";
    return await http.post(url, caseItem)
}

export async function addCase(caseItem) {
    let url = apiEndpoint + "/add";
    await http.post(url, caseItem);
}

export async function updateCase(item) {
    let url = apiEndpoint + "/update";
    await http.post(url, item);
}