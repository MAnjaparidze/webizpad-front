import http from './httpService';

const apiEndpoint = "/plans";

export async function getPlans(plansID) {
    let planItem = plansID;
    return await http.post(apiEndpoint, planItem,{
        mode: 'no-cors', // no-cors, *cors, sam
        headers: { 'Content-Type': 'application/json'},

    });
}

export async function getPlan(plansID) {
    let planItem = { id: plansID };
    let url = apiEndpoint + "/get";
    return await http.post(url, planItem,{
        mode: 'no-cors', // no-cors, *cors, sam
        headers: { 'Content-Type': 'application/json'},
    })
}

export async function addPlan(planItem) {
    let url = apiEndpoint + "/add";
    return await  (url, planItem,{
        mode: 'no-cors', // no-cors, *cors, sam
        headers: { 'Content-Type': 'application/json'},

    }).then(res => {return res.data}); 
}