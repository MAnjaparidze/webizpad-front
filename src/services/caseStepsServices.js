import http from './httpService';

const apiEndpoint = "/caseSteps";

export async function getCaseSteps(caseID) {
    let caseItem = { id: caseID };
    return await http.post(apiEndpoint, caseItem);
}

export async function getCaseStep(caseID) {
    let caseItem = { id: caseID };
    let url = apiEndpoint + "/get";
    return await http.post(url, caseItem)
}

export async function addCaseStep(caseItem) {
    let url = apiEndpoint + "/add";
    return await http.post(url, caseItem);
}

