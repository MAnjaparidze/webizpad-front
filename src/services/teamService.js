import http from './httpService';

const apiEndpoint = "/teams";

export function createTeam(user) {
    let url = apiEndpoint + "/add";
    let item = { author: user._id, name: user.teamname };
    return http.post(url, item);
}

export function getTeam(teamname) {
    let url = apiEndpoint + "/get";
    let teamObj = {
        teamname: teamname
    }
    return http.post(url, teamObj)
}

export async function addMember(obj) {
    let url = apiEndpoint + "/add_member";
    let item = { _id: obj.id, member: obj.member };
    return await http.put(url, item);
}

export async function addProject(obj) {
    let url = apiEndpoint + "/add_project";
    let item = { _id: obj.id, project: obj.project };
    return await http.put(url, item);
}