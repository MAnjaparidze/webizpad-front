"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.register = register;
exports.activateAccount = activateAccount;
exports.updateUserInfo = updateUserInfo;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.verifyResetToken = verifyResetToken;

var _httpService = _interopRequireDefault(require("./httpService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var apiEndpoint = "/users";

function register(user) {
  return _httpService["default"].post(apiEndpoint, {
    user: {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      teamname: user.teamname,
      isActivated: false,
      role: 1
    },
    isTeamMember: user.isTeamMember
  });
}

function activateAccount(token) {
  var url = apiEndpoint + "/activate-account";
  return _httpService["default"].post(url, {
    token: token
  });
}

function updateUserInfo(userObj) {
  var url = apiEndpoint + "/update";
  return _httpService["default"].put(url, userObj);
}

function forgotPassword(email) {
  var url = apiEndpoint + "/forgot-password";
  return _httpService["default"].put(url, {
    email: email
  });
}

function resetPassword(token, password) {
  var url = apiEndpoint + "/reset-password";
  return _httpService["default"].put(url, {
    token: token,
    password: password
  });
}

function verifyResetToken(token) {
  var url = apiEndpoint + "/verify-token";
  return _httpService["default"].put(url, {
    type: "reset",
    token: token
  });
}