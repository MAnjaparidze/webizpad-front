"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createTeam = createTeam;
exports.getTeam = getTeam;
exports.addMember = addMember;
exports.addProject = addProject;

var _httpService = _interopRequireDefault(require("./httpService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var apiEndpoint = "/teams";

function createTeam(user) {
  var url = apiEndpoint + "/add";
  var item = {
    author: user._id,
    name: user.teamname
  };
  return _httpService["default"].post(url, item);
}

function getTeam(teamname) {
  var url = apiEndpoint + "/get";
  var teamObj = {
    teamname: teamname
  };
  return _httpService["default"].post(url, teamObj);
}

function addMember(obj) {
  var url, item;
  return regeneratorRuntime.async(function addMember$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          url = apiEndpoint + "/add_member";
          item = {
            _id: obj.id,
            member: obj.member
          };
          _context.next = 4;
          return regeneratorRuntime.awrap(_httpService["default"].put(url, item));

        case 4:
          return _context.abrupt("return", _context.sent);

        case 5:
        case "end":
          return _context.stop();
      }
    }
  });
}

function addProject(obj) {
  var url, item;
  return regeneratorRuntime.async(function addProject$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          url = apiEndpoint + "/add_project";
          item = {
            _id: obj.id,
            project: obj.project
          };
          _context2.next = 4;
          return regeneratorRuntime.awrap(_httpService["default"].put(url, item));

        case 4:
          return _context2.abrupt("return", _context2.sent);

        case 5:
        case "end":
          return _context2.stop();
      }
    }
  });
}