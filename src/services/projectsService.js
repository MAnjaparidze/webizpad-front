import http from './httpService';

const apiEndpoint = "/projects";

export function getProjects(user) {
    let item = { userID: user._id};
    return http.post(apiEndpoint, item);
}

export function getProject(id) {
    let url = apiEndpoint + "/get";
    return http.post(url, {ProjectID: id})
}

export async function addProject(project) {
    let url = apiEndpoint + "/add";
    return await http.post(url, project);
}