import React, { useState, useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import jwtDecode from 'jwt-decode';

import RegisterPage from './components/Registration/index';
import LoginPage from './components/Login/index';
import ProjectsPage from './components/Projects/index';
import PlansPage from './components/Plans/index';
import PlanReportPage from './components/Plans/Report/ReportPage';
import ProjectPage from './components/Projects/ProjectPage/index';
import CasesPage from './components/Cases/index';
import CaseReportPage from './components/Cases/Report/ReportPage';
import ForgotPassword from './components/ResetPassword/ForgotPassword';
import ResetPassword from './components/ResetPassword/ResetPassword';
import ActivateAccount from './components/Registration/ActivateAccount';
import FreeSubscription from './components/Subscription/FreeSubscription';
import AccountNotActive from './components/Registration/AccountNotActive';

import UserSettings from './components/User/UserSettings';
import ManageUsers from './components/User/ManageUsers';

import { UserContext } from './contexts/UserContext';
import { activateAccount } from './services/userService';

import './App.css';

function App() {
  const [user, setUser] = useState(null);

  const history = useHistory();

  const handleUser = () => {
    const jwt = localStorage.getItem('token');
    const userObj = jwtDecode(jwt);
    setUser(userObj);
    checkSubs(userObj._date);
  }

  const checkSubs = (userTime) => {
    let now = Date.now();
    let usTime = Date.parse(userTime);

    let days = Math.floor((now - usTime) / (3600 * 1000 * 24));
    if (days === 14) {
      history.push("/free-sub")
    }
  }

  useEffect(() => {
    try {
      if (localStorage.getItem('token')) {
        handleUser();
      }
    } catch (ex) { }
  }, [])


  return (
    <div className="App">
      <Switch>

        <UserContext.Provider value={{ user, handleUser }}>
          <Route exact path='/user/manage-users' component={ManageUsers} />
          <Route exact path='/user/settings/' component={UserSettings} />
          <Route exact path='/cases/report/:id' component={CaseReportPage} />
          <Route exact path='/projects/:id/cases/:id' component={CasesPage} />
          <Route exact path='/plans/report/:id' component={PlanReportPage} />
          <Route exact path='/projects/:id/plans/:id' component={PlansPage} />
          <Route exact path='/projects/project/:id' component={ProjectPage} />
          <Route exact path='/projects' component={ProjectsPage} />

          <Route exact path='/reset-password/:id' component={ResetPassword} />
          <Route exact path='/forgot-password' component={ForgotPassword} />
          <Route exact path='/free-sub' component={FreeSubscription} />
          <Route exact path='/authentication/not-active' component={AccountNotActive} />
          <Route exact path='/authentication/activate/:id' component={ActivateAccount} />
          <Route exact path='/register' component={RegisterPage} />
          <Route exact path='/' component={LoginPage} />

        </UserContext.Provider>
      </Switch>
    </div >
  );
}

export default App;
