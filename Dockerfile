FROM node:12-alpine
RUN mkdir /frontend
WORKDIR /frontend
COPY package*.json ./
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build

CMD ["echo", " BUILD COMPLETE"]
